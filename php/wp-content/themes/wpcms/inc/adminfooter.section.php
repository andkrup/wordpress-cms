<?php
use wordpresscms\SectionManager;
$current = SectionManager::instance()->current();
?><script type="text/javascript" >
jQuery(document).ready(function($){
	SectionManager = function(){
	};

	SectionManager.prototype.handleEditorData = function(html, editor){
		box = $('#'+editor).find('div.inside');
		box.empty();
		box.append(html);
		
		var updatebuttons = $('a[rel=update-content]');
		updatebuttons.click(function(event){
			// TODO: this code is textarea/markitup/html specific
			var textarea = $('.markitup-editor');
			var content = textarea.val();

			var params = {action:'updateContent', handler:'textcontent', id: <?php echo $current->id()?>, postId: <?php echo $current->postId()?>, markup:'html', content:content};
			$.post(ajaxurl, params, function(data){
			}, 'json');
			return false;
		});
	};

	SectionManager.prototype.loadEditor = function(editor, params){
		var sectionmanager = this;
		var data = {action:'loadEditor', editor:editor, id: <?php echo $current->id()?>, postId: <?php echo $current->postId()?>};
		for(var k in params){
			data[k] = params[k];
		}
		$.post(ajaxurl, data, function(html){
			sectionmanager.handleEditorData(html, editor);
		}, 'html');
	};

	var sectionmanager = new SectionManager();
	
	var sectiontypeselect = $('select#cms_sectiontype-selector');
	sectiontypeselect.change(function(){
		var params = {
			type: sectiontypeselect.val()
		};
		sectionmanager.loadEditor('section-metabox', params);
	});
	
	sectionmanager.loadEditor('section-metabox', {type: sectiontypeselect.val()});
});
</script>
