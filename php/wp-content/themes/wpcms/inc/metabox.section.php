<?php
/**
 * Display the correct editor gui
 */

$current = $this->current();
$type = null;
if($current instanceof ContentSection){
	$type = $current->type()->name();
}
$mode = 'edit';

switch($type){
	default:
		echo "Select type first";
		break;
	case 'text':
		switch ($mode){
			case 'edit':
				include dirname(__FILE__) . "/editor.textarea.php";
				break;
		}
		break;
	case 'iframe':
		break;
}
?>