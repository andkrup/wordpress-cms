<?php
use wordpresscms\PhpScriptManager;
use wordpresscms\ContentSection;

// load existing text contents into textarea
$path = '';
$id = 0;
$current = PhpScriptManager::instance()->current();
if(isset($current)){
	$id = $current->id();
	if($current instanceof ContentSection){
		$current->fetch('edit');
		$path = $current->content();
	}
}
$inputId = "phpscript-sourcefilepath-$id";
?><dl>
	<dt><?php _e('Source file', 'reset')?>:</dt>
	<dd class="filepath phpscript-sourcefilepath"><input type="text" id="<?php echo $inputId?>" value="<?php echo $path?>"></dd>
	<dt><a class="button button-primary button-small" rel="update-content" href="#"><?php _e('Update Information', 'reset')?></a></dt>
</dl>