<?php
// scope is within PageManager
$currentPage = $this->current();
$sections = $currentPage->sections();
ksort($sections);
$pageId = $currentPage->id();
?><ul>
<?php foreach ($sections as $index=>$section):
	$id = $section->id();
	$clsname = end(explode("\\", get_class($section)));
	$iconDirUrl = get_template_directory_uri() . '/images/icons/16x16';
	$section->fetch('html');
?>
<li>
	<dl class="page-section">
		<dt>
			<dl class="section-options" data-section-id="<?php echo $id?>" data-section-priority="<?php echo $index?>">
				<dd><img class="icon" alt="move up <?php echo $clsname?>" src="<?php echo $iconDirUrl?>/arrow-up.png"></dd>
				<dd><img class="icon" alt="move down <?php echo $clsname?>" src="<?php echo $iconDirUrl?>/arrow-down.png"></dd>
				<dd><img class="icon" alt="edit <?php echo $clsname?>" src="<?php echo $iconDirUrl?>/pencil.png"></dd>
				<dd><img class="icon" alt="remove  <?php echo $clsname?>" src="<?php echo $iconDirUrl?>/remove.png">
			</dl>
		</dt>
		<dd><?php echo $section->content()?></dd>
	</dl>
</li>
<?php endforeach;?>
<li>
	<dl class="page-options" data-page-id="<?php echo $pageId?>">
		<dd><a href="#" rel="add new section">insert new...</a></dd>
		<dd><a href="#" rel="add existing section">insert existing...</a></dd>
	</dl>
</li>
</ul>