<?php
?><script type="text/javascript" >
jQuery(document).ready(function($){
	
	PageManager = function(){
	};

	/**
	 * Process the returned html snippet from a previous loadGui call - add it to the DOM and set up event handlers
	 */
	PageManager.prototype.handleGuiData = function(data, guiId, target, pageId, callback){
		var pagemanager = this;
		var parent = target.parent();
		var html = $(data);
		var add = $('<a class="inline-option" href="#">add</a>');
		add.click(function(event){
			var val = html.val();
			callback.apply(pagemanager, [pageId, val]);
			html.detach();
			add.detach();
			cancel.detach();
			parent.append(target);
			return false;
		});
		var cancel = $('<a class="inline-option" href="#">cancel</a>');
		cancel.click(function(event){
			html.replaceWith(target);
			add.detach();
			cancel.detach();
			return false;
		});
		target.before(html);
		target.after(cancel);
		target.after(add);
		target.detach();
	};

	/**
	 * Get some html from the backend. We're expecting a single html tag with everything nested underneath, suited to be made into a jQuery object 
	 */
	PageManager.prototype.loadGui = function(guiId, params, target, pageId, callback){
		var pagemanager = this;
		var data = {action:'loadSnippet', gui: guiId};
		$.post(ajaxurl, data, function(html){
			pagemanager.handleGuiData(html, guiId, target, pageId, callback);
		}, 'html');
		
	};

	/**
	 * Submit a sectionid to be added at current page
	 */
	PageManager.prototype.addSection = function(pageId, sectionId){
		console.log('addSection('+pageId+', '+sectionId+')');
		var data = {action:'addSection', sectionId: sectionId, pageId: pageId};
		$.post(ajaxurl, data, function(data){
			if(data.result == 'ok'){
				location.reload();
			}
			else{
				console.log('reply: '+data.result);
			}
		}, 'json');
	};

	PageManager.prototype.removeSection = function(pageId, sectionId){
		var data = {action:'removeSection', sectionId: sectionId, pageId: pageId};
		$.post(ajaxurl, data, function(data){
			if(data.result == 'ok'){
				location.reload();
			}
			else{
				console.log('reply: '+data.result);
			}
		}, 'json');
	};

	PageManager.prototype.swapSectionPriority = function(pageId, sectionId, oldPriority, newPriority, callback){
		var data = {action:'updatePriority', sectionId: sectionId, pageId: pageId, oldPriority: oldPriority, newPriority: newPriority};
		$.post(ajaxurl, data, function(data){
			callback.apply(null, [data]);
		}, 'json');
	};
	PageManager.prototype.onSwapSectionPriority = function(data){
		if(data.result == 'ok'){
			location.reload();
		}
		else{
			console.log('onSwapSectionPriority() data: '+data.result);
		}
	};

	PageManager.prototype.editSection = function(pageId, sectionId, callback){
		var data = {action:'editPriority', sectionId: sectionId, pageId: pageId};
		$.post(ajaxurl, data, function(data){
			callback.apply(null, [data]);
		}, 'json');
	};

	PageManager.prototype.onEditSection = function(data){
		if(data.result == 'ok'){
			location.reload();
		}
		else{
			console.log('onEditSection() data: '+data);
		}
	};

	var pagemanager = new PageManager();
	var pageOptions = $('dl[data-page-id]');
	var pageId = parseInt(pageOptions.attr('data-page-id'));
	var pageSections = $('dl.page-section');
	pageSections.find('img[alt^=move]').click(function(event){
		var icon = $(event.target);
		var section = icon.parent().parent();
		var oldPriority = parseInt(section.attr('data-section-priority'));
		var sectionId = parseInt(section.attr('data-section-id'));
		var newPriority = icon.attr('alt').toLowerCase().indexOf('move up') >= 0 ? (oldPriority <= 1 ? 0 : oldPriority - 1) : oldPriority + 1;
		console.log('newPriority: '+newPriority);
		pagemanager.swapSectionPriority(pageId, sectionId, oldPriority, newPriority, pagemanager.onSwapSectionPriority);
		return false;
	});
	pageSections.find('img[alt^=edit]').click(function(event){
		var icon = $(event.target);
		var sectionId = parseInt(icon.parent().parent().attr('data-section-id'));
		pagemanager.editSection(pageId, sectionId, pagemanager.onEditSection);
		return false;
	});
	pageSections.find('img[alt^=remove]').click(function(event){
		var icon = $(event.target);
		var sectionId = parseInt(icon.parent().parent().attr('data-section-id'));
		pagemanager.removeSection(pageId, sectionId, pagemanager.onRemoveSection);
		return false;
	});
	var addLink = $('a[rel="add existing section"]');
	addLink.click(function(event){
		pagemanager.loadGui('sections-select', {}, addLink, pageId, pagemanager.addSection);
		return false;
	});
	var addnewLink = $('a[rel="add new section"]');
	addnewLink.click(function(event){
		pagemanager.loadGui('section-create', {}, addLink, pageId, pagemanager.createSection);
		return false;
	});
});
</script>
