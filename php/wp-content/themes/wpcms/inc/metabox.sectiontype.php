<?php
$current = $this->current();
// available variables: $types
?><select id="cms_sectiontype-selector">
	<?php
	foreach ($types as $type){
		$name = $type->name;
		$description = $type->description;
		$selected = '';
		if(isset($current)){
			$ctype = $current->type();
			if(isset($ctype)){
				if($ctype->name() == $name){
					$selected = ' selected="selected"';
				}
			}
		}
		echo "<option$selected>$name</option>";
	}?>
</select>