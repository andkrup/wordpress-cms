<?php
$resetSrc = get_template_directory_uri() . '/images/icons/16x16/image.png';

?><script type="text/javascript">
	var imagemanagerOptions = {
			defaultImage : {
				src : '<?php echo $resetSrc?>'
			},
			constraints:{
				icon:{
					label: '<?php _e('Icon', 'reset')?>',
					width: 16,
					height: 16
				},
				button:{
					label: '<?php _e('Button', 'reset')?>',
					width: 24,
					height: 24
				}
			}
	};
</script>
<script type="text/javascript">
jQuery(document).ready(function($){
	ImageManager = function(options){
		this.options = options;
		this.targetConstraints = null;
		var instance = this;
		
		this.previewIcons = $('img[alt^="preview"]');
		this.openmediaIcons = $('img[alt^="select"]');
		this.removeIcons = $('img[alt^="remove"]');
		
		this.openmediaIcons.click(function(event){
			instance.setTargetId($(event.target).parent().parent().attr('data-target-id'));
			instance.setTargetConstraints($(event.target).parent().parent().attr('data-target-contraints'));
			window.send_to_editor = function(html){
				instance.handleTbSelection(html, instance);
			};
			tb_show('Upload/Select an icon', 'media-upload.php?type=image&TB_iframe=true&post_id=0', false);
		});

		this.removeIcons.click(function(event){
			var targetId = instance.setTargetId($(event.target).parent().parent().attr('data-target-id'));
			$('#' + targetId).attr('src', instance.options.defaultImage.src);
		});
	};

	ImageManager.prototype.getTargetId = function(){
		return this.targetId;
	};
	ImageManager.prototype.setTargetId = function(id){
		this.targetId = id;
		return id;
	};
	ImageManager.prototype.getTargetConstraints = function(){
		return this.targetConstraints;
	};
	ImageManager.prototype.setTargetConstraints = function(constraint){
		for(var k in this.options.constraints){
			if(k == constraint){
				this.targetConstraints = this.options.constraints[k];
				console.log(this.targetConstraints);
// 				return;
			}
		}
	};
	ImageManager.prototype.getSelectedSrc = function(){
		return this.targetSrc;
	};
	ImageManager.prototype.setSelectedSrc = function(src){
		this.targetSrc = src;
	};
	
	ImageManager.prototype.handleTbSelection = function(html, self){
		var image_url = $('img',html).attr('src');
		self.setSelectedSrc(image_url);
		var target = $('#' + self.getTargetId());
		target.hide();
		target.attr('src', image_url);
		var messages = [];
		if(null!=this.targetConstraints){
			if(this.targetConstraints.width != target.width()){
				messages.push('The width will not fit the constraints for a '+this.targetConstraints.label+' in the design. The image width is '+target.width()+' but should be '+this.targetConstraints.width);
			}
			if(this.targetConstraints.height != target.height()){
				messages.push('The width will not fit the constraints for a '+this.targetConstraints.label+' in the design. The image height is '+target.height()+' but should be '+this.targetConstraints.height);
			}
		}
		if(messages.length>0){
			var str = '';
			for(var k in messages){
				str += messages[k] + "\n";
			}
			alert(str);
			target.attr('src', this.options.defaultImage.src);
		}
		target.fadeIn(400);
		tb_remove();
		delete window.send_to_editor;
	};

	var imagemanager = new ImageManager(imagemanagerOptions);
});
</script>