<?php
global $wpdb;
$sql = "select s.id as section_id, post_title from cms_sections s
left join wp_posts p on p.ID = s.post_id
left join cms_sectiontypes st on st.id = s.type_id";
$sections = $wpdb->get_results($sql);
?><select>
<?php foreach ($sections as $section){
	echo "<option value=\"{$section->section_id}\">{$section->post_title}</option>";
}?>
</select>