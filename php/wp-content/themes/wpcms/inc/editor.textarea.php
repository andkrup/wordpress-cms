<?php
use wordpresscms\SectionManager;
use wordpresscms\ContentSection;

// load existing text contents into textarea
$text = '';
$id = 0;
$current = SectionManager::instance()->current();
if(isset($current)){
	$id = $current->id();
	if($current instanceof ContentSection){
		$current->fetch('edit');
		$text = $current->content();
	}
}
$textareaId = "text-content-$id";
?><dl>
	<dt><?php _e('Text Content', 'reset')?></dt>
	<dd><textarea class="markitup-editor" id="<?php echo $textareaId?>" data-section-id="<?php echo $id?>"><?php echo $text?></textarea></dd>
	<dt><a class="button button-primary button-small" rel="update-content" href="#"><?php _e('Update Content', 'reset')?></a></dt>
</dl>

<script type="text/javascript" >
	jQuery(document).ready(function($) {
		$("#<?php echo $textareaId?>").markItUp(mySettings);
	});
</script>
