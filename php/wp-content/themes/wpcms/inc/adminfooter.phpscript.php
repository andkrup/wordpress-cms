<?php
use wordpresscms\PhpScriptManager;
$current = PhpScriptManager::instance()->current();
?><script type="text/javascript" >
jQuery(document).ready(function($){
	PhpScriptManager = function(){
		this.loadEditor('phpscript-metabox', {type: 'phpscript'}, this.handleEditorData);
	};

	PhpScriptManager.prototype.loadEditor = function(editor, params, callback){
		var phpscriptmanager = this;
		var data = {action:'loadEditor', editor: editor, id: <?php echo $current->id()?>, postId: <?php echo $current->postId()?>};
		for(var k in params){
			data[k] = params[k];
		}
		$.post(ajaxurl, data, function(html){
			callback.apply(phpscriptmanager, [html, editor]);
		}, 'html');
	};
	
	PhpScriptManager.prototype.handleEditorData = function(html, editor){
		var phpscriptmanager = this;
		box = $('#'+editor).find('div.inside');
		box.empty();
		box.append(html);
		$('a[rel=update-content]').click(function(event){
			var srcinput = $('dd.phpscript-sourcefilepath>input[type=text]');
			var value = srcinput.val();
			console.log('update content: '+value);
			var params = {action:'updateContent', handler:'phpscript', id: <?php echo $current->id()?>, postId: <?php echo $current->postId()?>, path:value};
			$.post(ajaxurl, params, function(data){
				if(data.result == 'ok'){
					phpscriptmanager.loadEditor('phpscript-metabox', {type: 'phpscript'}, phpscriptmanager.handleEditorData);
				}
				else{
					console.log(data);
				}
			}, 'json');
			return false;
		});
	};

	var phpscriptmanager = new PhpScriptManager();
});
</script>
