<?php
$default_headers = array(
		'Name' => 'Template Name',
		'Version' => 'Version',
		'Description' => 'Description',
		'Author' => 'Author',
		'CmsPlaceholders' => 'CMS Placeholders',
		'CmsPlaceholderThumb' => 'CMS PlaceholderThumb'
);
$templates = get_page_templates();
?><div class="inside pagetemplate-box">
	<h4>Select Template</h4>
	<ul>
		<?php foreach ($templates as $template):?>
		<?php
			// fetch header-info for each template file
			$fileinfo = (object) get_file_data("/var/www/wp-content/themes/cms-theme/$template", $default_headers);
		?>
		<li>
			<article>
				<h3><?php echo $fileinfo->Name;?></h3>
				<img alt="" src="/wp-content/themes/cms-theme/<?php echo $fileinfo->CmsPlaceholderThumb;?>">
			</article>
		</li>
		<?php endforeach;?>
	</ul>
	<p class="howto">Click to select</p>
</div>