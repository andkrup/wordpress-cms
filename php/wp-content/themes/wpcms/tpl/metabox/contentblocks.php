<?php
// $default_headers = array(
// 		'Name' => 'Template Name',
// 		'Version' => 'Version',
// 		'Description' => 'Description',
// 		'Author' => 'Author',
// 		'CmsPlaceholders' => 'CMS Placeholders',
// 		'CmsPlaceholderThumb' => 'CMS PlaceholderThumb'
// );
// $templates = get_page_templates();
$currentTemplate = cms_get_template($_GET['post']);
$placeholders = cms_get_placeholders($currentTemplate);
?><div class="inside placeholders-box" style="overflow: auto;">
	<?php foreach ($placeholders as $placeholder):?>
	<section class="placeholder-editor" style="width: 30%; height: 600px; border: 1px solid #000; float: left; overflow: auto; margin: 6px;">
		<h4>editor for placeholder <?php echo $placeholder;?></h4>
	</section>
	<?php endforeach;?>
</div>