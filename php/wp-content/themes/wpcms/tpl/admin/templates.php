<?php
$default_headers = array(
		'Name' => 'Template Name',
		'Version' => 'Version',
		'Description' => 'Description',
		'Author' => 'Author',
		'CmsPlaceholders' => 'CMS Placeholders',
		'CmsPlaceholderThumb' => 'CMS PlaceholderThumb'
);
$templates = get_page_templates();
?><div class="wrap">
	<h2>Templates</h2>
	<p>A list of all available templates in this theme</p>
	<?php foreach ($templates as $template):?>
	<?php
		// fetch header-info for each template file
		$fileinfo = (object) get_file_data("/var/www/wp-content/themes/cms-theme/$template", $default_headers);
// 		print_r($fileinfo);
	?>
		<article>
			<h3><?php echo $fileinfo->Name;?></h3>
			<p><?php echo $fileinfo->Description;?></p>
			<dl>
				<dt>Placeholders</dt>
				<dd><?php echo $fileinfo->CmsPlaceholders;?></dd>
				<dd><img alt="" src="/wp-content/themes/cms-theme/<?php echo $fileinfo->CmsPlaceholderThumb;?>"></dd>
			</dl>
		</article>
	<br>
	<?php endforeach;?>
</div>