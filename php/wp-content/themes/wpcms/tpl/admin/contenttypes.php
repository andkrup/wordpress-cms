<?php
$ctypes = ContentTypeManager::listAll();
?><div class="wrap">
	<h2>Content-Types</h2>
	<p>What kind of content can I put into my pages?</p>
	<table border="1">
		<thead>
			<tr>
				<th>Name</th>
				<th>Description</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($ctypes as $ctype):?>
			<tr>
				<td><?php echo $ctype->name;?></td>
				<td><?php echo $ctype->description;?></td>
			</tr>
			<?php endforeach;?>
		</tbody>
	</table>
</div>