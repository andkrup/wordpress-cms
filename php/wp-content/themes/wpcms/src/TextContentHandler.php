<?php
namespace wordpresscms{
	class TextContentHandler{
		static function updateContent(TextContent $section, $content, $markup, $language = 'en_US'){
			$plaintext = '';
			switch($markup){
				case 'html':
					$plaintext = preg_replace('/<(.*?)>/', '', $content);
					break;
			}
				
			global $wpdb;
			$wpdb->flush();
			if(is_null($section->contentId())){
				// create new row
				$sql = "insert into cms_textcontent(section_id, locale_id, plaintext, html, editor, editor_id)
					values(%d, (select id from cms_locales where code = %s), %s, %s, %s, 1) on duplicate key update id = LAST_INSERT_ID(id);";
				$wpdb->query($wpdb->prepare($sql, $section->id(), $language, $plaintext, $content, $content));
				$section->setContentId($wpdb->insert_id);
				$section->save();
			}
			else{
				// update existing
				$sql = "update cms_textcontent set
					locale_id = (select id from cms_locales where code = %s),
					plaintext = %s,
					html = %s,
					editor = %s,
					editor_id = 1
					where id = %d;";
				$wpdb->query($wpdb->prepare($sql, $language, $plaintext, $content, $content, $section->contentId()));
			}
			return $wpdb->last_error;
		}
	}
}