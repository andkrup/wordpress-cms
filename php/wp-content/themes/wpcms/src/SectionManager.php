<?php
namespace wordpresscms{
	class SectionManager{
		private $current;
		private static $inst;
		
		public static function instance(){
			if(!isset(self::$inst)){
				self::$inst = new SectionManager();
			}
			return self::$inst;
		}
		
		private function __construct(){
		}
		
		function current(){
			return $this->current;
		}
		function setCurrent(Section $current){
			$this->current = $current;
		}
		
		public function displayMainSectionMetabox(){
			include dirname(__FILE__)."/../inc/metabox.section.php";
		}
		
		public function displaySectionTypeMetabox(){
			$sql = "select name, description from cms_sectiontypes;";
			global $wpdb;
			$types = $wpdb->get_results($sql);
			include dirname(__FILE__)."/../inc/metabox.sectiontype.php";
		}
		
		/**
		 * When a Section wordpress post has been created, we also need a new cms_section row
		 * @param unknown $post_id
		 */
		static function onSaveSection($postId){
			$section = Section::create($postId);
			self::instance()->setCurrent($section);
		}
		/**
		 * All sectiontypes are Sections
		 * @param unknown $postId
		 */
		static function deleteSection($postId){
			global $wpdb;
			$wpdb->query($wpdb->prepare("delete from cms_sections where post_id = %d;", $postId));
		}
		
		static public function onAdminFooter(){
			global $post;
			if(isset($post) && $post->post_type == 'section'){
				include dirname(dirname(__FILE__))."/inc/adminfooter.section.php";
			}
		}
	}
}