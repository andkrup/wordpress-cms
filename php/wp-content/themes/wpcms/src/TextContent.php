<?php
namespace wordpresscms{
	class TextContent extends Section implements ContentSection{
		private $mode;
		private $text;
		
		function content(){
			return $this->text;
		}
		function setContent($content){
			$this->text = $content;
		}
		function mode(){
			return $this->mode;
		}
		function setMode($mode){
			switch ($mode){
				case 'edit':
				case 'plain':
				case 'html':
					$this->mode = $mode;
					break;
			}
		}
		
		function fetch($mode){
			global $wpdb;
			switch ($mode){
				case 'edit':
					$sql = "select editor as text
					from cms_sections s
					left join cms_textcontent tc on tc.id = s.content_id and tc.section_id = s.id
					where s.id = %d";
					break;
				case 'plain':
					$sql = "select plaintext as text
					from cms_sections s
					left join cms_textcontent tc on tc.id = s.content_id and tc.section_id = s.id
					where s.id = %d";
					break;
				case 'html':
					$sql = "select html as text
					from cms_sections s
					left join cms_textcontent tc on tc.id = s.content_id and tc.section_id = s.id
					where s.id = %d";
					break;
			}
			$row = $wpdb->get_row($wpdb->prepare($sql, $this->id));
			$this->text = $row->text;
		}
	}
}