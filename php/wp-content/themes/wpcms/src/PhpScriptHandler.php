<?php
namespace wordpresscms{
	class PhpScriptHandler{
		static function updateContent(PhpScript $phpscript, $path){
			global $wpdb;
			$wpdb->flush();
			$contentId = $phpscript->contentId();
			if(is_null($contentId) || 0 === $contentId){
				// create new row
				$sql = "insert into cms_phpcontent(section_id, path) values(%d, %s) on duplicate key update id = LAST_INSERT_ID(id);";
				$wpdb->query($wpdb->prepare($sql, $phpscript->id(), $path));
				$phpscript->setContentId($wpdb->insert_id);
				$phpscript->save();
			}
			else{
				// update existing
				$sql = "update cms_phpcontent set path = %s where id = %d;";
				$wpdb->query($wpdb->prepare($sql, $path, $phpscript->contentId()));
			}
			return $wpdb->last_error;
		}
	}
}