<?php
namespace wordpresscms{
	class IFrameContent extends Section implements ContentSection{
		private $mode;
		private $src;
		
		function content(){
			return $this->src;
		}
		function setContent($content){
			$this->src = $content;
		}
		function mode(){
			return $this->mode;
		}
		function setMode($mode){
			switch ($mode){
				case 'edit':
				case 'plain':
				case 'html':
					$this->mode = $mode;
					break;
			}
		}
		
		function fetch($mode){
			global $wpdb;
			switch ($mode){
				case 'edit':
					break;
				case 'plain':
					break;
				case 'html':
					break;
			}
			$this->src = '';
		}
	}
}
