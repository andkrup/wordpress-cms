<?php
namespace wordpresscms{
	class PhpScriptManager{
		private $current;
		private static $inst;
	
		public static function instance(){
			if(!isset(self::$inst)){
				self::$inst = new PhpScriptManager();
			}
			return self::$inst;
		}
		
		private function __construct(){}

		function current(){
			return $this->current;
		}
		function setCurrent(PhpScript $current){
			$this->current = $current;
		}
		
		public function displayMainPhpScriptMetabox(){
			include dirname(__FILE__).'/../inc/metabox.phpscript.php';
		}
		
		/**
		 * When a PhpScript wordpress post has been created, we also need a new cms_phpscript row
		 * @param unknown $post_id
		 */
		static function onSavePhpScript($postId){
			$section = PhpScript::create($postId);
			self::instance()->setCurrent($section);
		}
		
		static public function onAdminFooter(){
			global $post;
			if(isset($post) && $post->post_type == 'phpscript'){
				include dirname(dirname(__FILE__))."/inc/adminfooter.phpscript.php";
			}
		}
	}
}