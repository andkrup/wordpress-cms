<?php
namespace wordpresscms{
	require_once dirname(__FILE__) . "/../lib/common/Output.php";
	
	class EditorManager{
		private $editors;
		private static $inst;
		
		public static function instance(){
			if(!isset(self::$inst)){
				self::$inst = new EditorManager();
			}
			return self::$inst;
		}
		
		private function __construct(){
			$this->clearEditors();
		}
		
		function addEditor(){}
		function removeEditor(){}
		function containsEditor(){}
		function clearEditors(){
			$this->editors = array();
		}
		
		public function displayOptions(){
			include dirname(__FILE__)."/../tpl/admin/editor.options.php";
		}
		
		static function onRequestEditorGui(){
			$buffer = '';
			$postId = intval($_POST['postId']);
			$current = Section::load($postId);
			$editor = trim($_POST['editor']);
			switch ($_POST['type']){
				case 'text':
					SectionManager::instance()->setCurrent($current);
					ob_start();
					include dirname(__FILE__) . '/../inc/editor.textarea.php';
					$buffer = ob_get_clean();
					break;
				case 'iframe':
					ob_start();
					include dirname(__FILE__) . '/../inc/editor.iframe.php';
					$buffer = ob_get_clean();
					break;
				case 'button':
					ob_start();
					include dirname(__FILE__) . '/../inc/editor.button.php';
					$buffer = ob_get_clean();
					break;
				case 'phpscript':
					if(!isset($current)){
						$current = new PhpScript($postId);
					}
					PhpScriptManager::instance()->setCurrent($current);
					ob_start();
					include dirname(__FILE__) . '/../inc/editor.phpscript.php';
					$buffer = ob_get_clean();
					break;
			}
			\common\wordpress\Output::printHtml($buffer);
		}
		
		static function onRequestGuiSnippet(){
			ob_start();
			include dirname(__FILE__) . '/../inc/gui.sections.select.php';
			$buffer = ob_get_clean();
			\common\wordpress\Output::printHtml($buffer);
		}
		
		static function onUpdateContent(){
			$postId = intval($_POST['postId']);
			switch($_POST['handler']){
				case 'textcontent':
					$section = Section::load($postId);
					$content = trim($_POST['content']);
					$markup = trim($_POST['markup']);
					$result = TextContentHandler::updateContent($section, $content, $markup);
					echo json_encode((object) array('result' => strlen($result) === 0 ? 'ok' : $result));
					break;
				case 'phpscript':
					$path = trim($_POST['path']);
					$section = Section::load($postId);
					$result = PhpScriptHandler::updateContent($section, $path);
					echo json_encode((object) array('result' => strlen($result) === 0 ? 'ok' : $result));
					break;
			}
			die();
		}
	}
}
?>