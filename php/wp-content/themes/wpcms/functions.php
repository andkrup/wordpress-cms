<?php
namespace wordpresscms;

/**
 * Autoloader function
 * @param string $classPath absolute class path (including class name)
 * @see http://andkrup.wordpress.com/2013/04/10/autoloader-for-wordpress/
 */
function svhq_load($classPath){
	$base = dirname(__FILE__);
	$paths = array(
		"$base/src"
	);
	foreach($paths as $path){
		$filename = str_replace("\\", '/', "$path/" . __NAMESPACE__ . $classPath . '.php');
		if(is_readable($filename)){
			require $filename;
		}
	}
}
spl_autoload_register(__NAMESPACE__."\\svhq_load");

require_once WP_CONTENT_DIR . '/vendor/autoload.php';

use net\supervillainhq\wordpressreset\Reset;

/**
 * Deflate Wordpress bloat
 */
//global $wpcmsreset;
$wpcmsreset = Reset::instance();

// run this when activating this theme
add_action('after_switch_theme', array($wpcmsreset, 'onActivation'));
// run this on every REQUEST
add_action('init', array($wpcmsreset, 'onInit'));
add_action('wp_enqueue_scripts', array($wpcmsreset, 'onEnqueueScripts'));

/**
 * Add admin features
 */
if(is_admin()){
	add_action('admin_menu', array($wpcmsreset, 'onAdminMenu'));
}


/**
 * Alter Wordpress to meet our needs
 */

/**
 * Add admin features
 */
if(is_admin()){
	// TODO: only output the scripts you need, not all at once
//	add_action('admin_footer', array('wordpresscms\\PageManager', 'onAdminFooter'));
//	add_action('admin_footer', array('wordpresscms\\SectionManager', 'onAdminFooter'));
//	add_action('admin_footer', array('wordpresscms\\PhpScriptManager', 'onAdminFooter'));
//	add_action('admin_footer', array('wordpresscms\\ImageManager', 'onAdminFooter'));
//	// ajax frontend calls (admin)
//	add_action('wp_ajax_loadEditor', array('wordpresscms\\EditorManager', 'onRequestEditorGui')); # return the html for displaying the selected editor
//	add_action('wp_ajax_loadSnippet', array('wordpresscms\\EditorManager', 'onRequestGuiSnippet')); # return the html for displaying the selected snippet
//	add_action('wp_ajax_updateContent', array('wordpresscms\\EditorManager', 'onUpdateContent')); # save/update content for the selected section
//	add_action('wp_ajax_addSection', array('wordpresscms\\PageManager', 'onAddExistingSection')); # create a relation between a Section and a Page
//	add_action('wp_ajax_removeSection', array('wordpresscms\\PageManager', 'onRemoveSection')); # remove a relation between a Section and a Page
//	add_action('wp_ajax_updatePriority', array('wordpresscms\\PageManager', 'onUpdatePriority')); # change the ordering of Section instances in a Page
}
//global $wpcmscl;
//$wpcmscl = client\WordpressCmsClient::instance();
//add_action('wp_enqueue_scripts', array($wpcmscl, 'onEnqueueScripts'));
//add_action('plugins_loaded', array($wpcmscl, 'onPluginsLoaded'));
//add_action('after_setup_theme', array($wpcmscl, 'onSetupTheme'));
//add_action('wp', array($wpcmscl, 'onWP'));
//add_action('wp_head', array($wpcmscl, 'onWpHead'));
//add_action('init', array($wpcmscl, 'onInit'));
//add_action('wpcms_print_scriptliterals', array($wpcmscl, 'onPrintScriptLiterals'));
//// ajax frontend calls (front-end)
//add_action('wp_ajax_getPage', array($wpcmscl, 'onGetPage')); # return the html for a specific Page
//add_action('wp_ajax_nopriv_getPage', array($wpcmscl, 'onGetPage')); # return the html for a specific Page
//add_action('wp_ajax_getSection', array($wpcmscl, 'onGetSection')); # return the html for a specific Section within a specific Page
//add_action('wp_ajax_nopriv_getSection', array($wpcmscl, 'onGetSection')); # return the html for a specific Section within a specific Page
//add_action('init', array('wordpresscms\\PageManager', 'onInit'));

