/**
 * Ajax functionality for the reset theme
 */

var wpcmsajax;

$(document).ready(function(){
	/**
	 * WpCmsAjax handles all ajax duties
	 */
	WpCmsAjax = function(){
	};
	
	WpCmsAjax.prototype.fetch = function(action, params, callback){
		var scope = this;
		var args = {action: action};
		$.post(ajaxurl, args, function(json){
			if(typeof json.result != 'undefined' && json.result == 'ok'){
				callback.apply(scope, json);
			}
			else{
				if(typeof json.message != 'undefined'){
					console.log('message: '+json.message);
				}
				else{
					console.log('unknown error');
				}
			}
		}, 'json');
	};
	/**
	 * Fetch all html for a specific page object
	 */
	WpCmsAjax.prototype.getPage = function(pageid){
		this.fetch('getPage', {page: pageid}, this.onGetPage);
	};
	WpCmsAjax.prototype.onGetPage = function(html){
		// handle data
	};
	
	/**
	 * Fetch all html for a specific section object, given a page object
	 */
	WpCmsAjax.prototype.getSection = function(sectionid, pageid){
		
	};
	WpCmsAjax.prototype.onGetSection = function(html){
		// handle data
	};
	
	/**
	 * Global instance; should be namespaced with the wpcms object (if created ???)
	 */
	wpcmsajax = new WpCmsAjax();
	wpcmsajax.getPage(2);
});
