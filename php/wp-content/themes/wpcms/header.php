<?php
?><!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title><?php wp_title();?></title>
		<style type="text/css">
			@import "css/reset.css";
			@import "css/layout.css";
		</style>
		<?php wp_head();?>
	</head>
