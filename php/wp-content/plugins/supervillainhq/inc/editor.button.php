<?php
$parentthemepath = get_template_directory_uri();

$id = 0;
$previewId = "icon-preview-$id";
$inputId = "button-linkpath-$id";
?><dl>
	<dt><?php _e('icon', 'reset')?>:</dt>
	<dd>
		<img id="preview-<?php echo $previewId?>" alt="preview icon" src="<?php echo $parentthemepath;?>/images/icons/16x16/image.png">
		<dl class="image-options" data-target-id="<?php echo $previewId?>" data-target-contraints="button">
			<dd><img class="icon" alt="select icon" src="<?php echo $parentthemepath;?>/images/icons/16x16/folder-open.png"></dd>
			<dd><img class="icon" alt="remove icon" src="<?php echo $parentthemepath;?>/images/icons/16x16/remove2.png"></dd>
		</dl>
	</dd>
	<dt><?php _e('target link', 'reset')?>:</dt>
	<dd class="internetpath button-linkpath"><input type="text" id="linkpath-<?php echo $inputId?>"></dd>
	<dt><a class="button button-primary button-small" rel="update-content" href="#"><?php _e('Update Information', 'reset')?></a></dt>
</dl>