<?php
use wordpresscms\ButtonManager;
$current = ButtonManager::instance()->current();
?><script type="text/javascript">
jQuery(document).ready(function($){
	ButtonManager = function(){
		var instance = this;
		$('a[rel=update-content]').click(function(event){
			var targetId = $('dl[data-target-id]').attr('data-target-id');
			var targetUrl = $('input#linkpath-'+targetId).val();
			var iconSrc = $('img#preview-'+targetId).attr('src');
			var params = {action:'updateContent', handler:'button', id: <?php echo $current->id()?>, postId: <?php echo $current->postId()?>, targetUrl:targetUrl, iconSrc:iconSrc};
			console.log('update content: '+params);
			instance.update(params);
			return false;
		});
	};
	
	ButtonManager.prototype.loadEditor = function(editor, params, callback){
		var instance = this;
		var data = {action:'loadEditor', editor: editor, id: <?php echo $current->id()?>, postId: <?php echo $current->postId()?>};
		for(var k in params){
			data[k] = params[k];
		}
		$.post(ajaxurl, data, function(html){
			callback.apply(instance, [html, editor]);
		}, 'html');
	};
	ButtonManager.prototype.onEditor = function(html, editor){
		box = $('#'+editor).find('div.inside');
		box.empty();
		box.append(html);
	};
	
	ButtonManager.prototype.update = function(params){
		var instance = this;
		$.post(ajaxurl, params, function(data){
			if(data.result == 'ok'){
				instance.loadEditor('button-metabox', {type: 'button'}, instance.onEditor);
			}
			else{
				console.log(data);
			}
		}, 'json');
	};

	var buttonmanager = new ButtonManager();
});

</script>