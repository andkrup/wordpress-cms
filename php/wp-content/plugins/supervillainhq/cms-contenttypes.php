<?php
/*
 * Plugin Name: CMS Contenttypes
 * Plugin URI: http://supervillainhq.net
 * Description: Companion CMS Contenttypes for CMS
 * Author: SupervillainHQ
 * Version: 0.1
 * Author URI: http://supervillainhq.net
 */
$path = dirname(__FILE__);
require_once "$path/src/Button.php";
if(is_admin()){
	require_once "$path/src/ButtonManager.php";
}

register_activation_hook(__FILE__, array('wordpresscms\\plugins\\ButtonManager', 'onActivation'));

add_action('init', array('wordpresscms\\plugins\\WordpressCMS', 'onInit'));
if(is_admin()){
	add_action('add_meta_boxes_button', array('wordpresscms\\plugins\\ButtonManager', 'onAddButtonMetaBoxes'), 10, 1);
	add_action('admin_footer', array('wordpresscms\\plugins\\ButtonManager', 'onAdminFooter'));
	add_action('save_post', array('wordpresscms\\plugins\\ButtonManager', 'onSavePost'));
	add_action('before_delete_post', array('wordpresscms\\plugins\\ButtonManager', 'onDeletePost'));
}