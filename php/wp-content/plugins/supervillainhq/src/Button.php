<?php
namespace wordpresscms\plugins{
	class Button extends Section implements ContentSection{
		private $iconpath;
		private $linkpath;
		private $buffer;
		
		function fetch($mode){
			switch ($mode){
				case 'edit':
				case 'plain':
					break;
				case 'html':
					$this->buffer = "<a href=\"$this->linkpath\"><img alt=\"\" src=\"$this->iconpath\" ></a>\n";
					break;
			}
		}
		
		function content(){
			if(isset($this->buffer)){
				return $this->buffer;
			}
			return (object) array('icon' => $this->iconpath, 'link' => $this->linkpath);
		}
		function setContent($content){
			$this->iconpath = (object) $content->icon;
			$this->linkpath = (object) $content->link;
		}
		
		static function displayMainButtonMetabox(){
			include dirname(__FILE__)."/../inc/metabox.button.php";
		}
			
		static function create($postId){
			global $wpdb;
			$wpdb->query($wpdb->prepare("insert into cms_sections(post_id, type_id) values(%d, 3) on duplicate key update id = LAST_INSERT_ID(id);", $postId));
			return new PhpScript($wpdb->insert_id, $postId, new SectionType(3));
		}
	}
}