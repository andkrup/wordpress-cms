<?php
namespace wordpresscms\plugins{
	class ButtonManager{
		private $current;
		private static $inst;
	
		public static function instance(){
			if(!isset(self::$inst)){
				self::$inst = new ButtonManager();
			}
			return self::$inst;
		}
		
		private function __construct(){}

		function current(){
			return $this->current;
		}
		function setCurrent(Button $current){
			$this->current = $current;
		}
		
		static function onActivation(){
			global $wpdb;
			// (re)create tables
			$sql = "";
			// update types table
			$sql = "";
		}
		
		static function onInit(){
			// add buttons
			$labels = array('name'=> "Buttons", 'singular_name' => "Button", 'add_new' => "New Button");
			$properties = array('label' => "Button",
					'labels' => $labels,
					'public' => false,
					'has_archive' => true,
					'description' => "Button",
					'exclude_from_search' => true,
					'show_ui' => true,
					'show_in_menu' => true,
					'show_in_nav_menus' => true,
					'register_meta_box_cb' => array(self::instance(), 'onRegisterButtonMetaboxes'),
					'supports' => array('title')
			);
			register_post_type('button', $properties);
		}
		public function onRegisterButtonMetaboxes(){
			wp_enqueue_style('thickbox');
			wp_enqueue_script('thickbox');
		}
		
		static function onAddButtonMetaBoxes($post){
			$buttonmanager = ButtonManager::instance();
			$current = Section::load(intval($post->ID));
			if(!isset($current)){
				// create new
				$current = new Button(intval($post->ID));
			}
			$buttonmanager->setCurrent($current);
			add_meta_box('button-metabox', 'Basic properties', array('wordpresscms\\Button', 'displayMainButtonMetabox'), 'button', 'normal', 'high');
		}
		
		static function onSavePost($postId){
			if(array_key_exists('post_type', $_POST) && $_POST['post_type'] == 'button'){
			}
		}
		static function onDeletePost($postId){
			if(array_key_exists('post_type', $_POST) && $_POST['post_type'] == 'button'){
			}
		}
		
		static public function onAdminFooter(){
			global $post;
			if(isset($post) && $post->post_type == 'button'){
				include dirname(dirname(__FILE__))."/inc/adminfooter.button.php";
			}
		}
	}
}