<?php
return new \Phalcon\Config([
	'database' => [
		'adapter' => 'Mysql',
		'host' => 'localhost',
		'username' => 'vagrant',
		'password' => 'vagrant',
		'dbname' => 'WordpressCMS',
	]
]);
