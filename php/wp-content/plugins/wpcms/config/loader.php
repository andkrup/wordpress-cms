<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs([

])->register();

$loader->registerNamespaces([
    	'supervillainhq\\net' => WP_CONTENT_DIR . '/plugins/wpcms/src',
    ])->register();

require_once WP_CONTENT_DIR . '/vendor/autoload.php';