<?php

use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\View\Simple;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use \Phalcon\Mvc\Dispatcher as PhDispatcher;


/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();

$di->set('config', $config);


/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function () use ($config) {
    return new DbAdapter(array(
        'host' => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname' => $config->database->dbname,
        "options" => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        )
    ));
});

$di->set('view', function () {
	$view = new Simple();
	$view->setViewsDir(dirname(__FILE__) . '/../views/');
	return $view;
}, true);

/**
 * Start the session the first time some component request the session service
 */
$di->set('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});

$di->set('controller', function($request, $parameters) use ($di){
	if(is_admin()){
		$factory = new \net\supervillainhq\wordpresscms\controllers\AdminPageControllerFactory($di, $request);
	}
	else{
		$factory = new \net\supervillainhq\wordpresscms\controllers\PageControllerFactory($di, $request);
	}
	return $factory->getController($parameters);
});



//Register the flash service with custom CSS classes
$di->set('flash', function () {
    $flash = new \Phalcon\Flash\Direct(array(
        'error' => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice' => 'alert alert-info',
        'warning' => 'alert alert-warning'
    ));
    return $flash;
});

$di->set('flashSession', function () {
    $flash = new \Phalcon\Flash\Session(array(
        'error' => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice' => 'alert alert-info',
        'warning' => 'alert alert-warning'
    ));
    return $flash;
});



