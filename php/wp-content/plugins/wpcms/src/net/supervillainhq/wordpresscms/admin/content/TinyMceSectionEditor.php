<?php
namespace net\supervillainhq\wordpresscms\admin\content{
	use net\supervillainhq\wordpresscms\content\Section;
	use net\supervillainhq\wordpresscms\admin\ContentEditor;
use net\supervillainhq\wordpresscms\dao\SectionQuery;
			
	class TinyMceSectionEditor extends SectionEditor implements ContentEditor{
		const EDITOR_NAME = "tmce_editor";
		protected $wpEditorOptions;
		protected $settings;
		protected $htmlId;
		protected $htmlName;
		
		function __construct(Section $section, array $settings = null, $htmlName = null, $allowMedia = false){
			parent::__construct($section);
			
			if(is_null($settings)){
				$this->settings = array(
					'toolbar1'=> 'bold,italic,strikethrough,bullist,numlist,blockquote,link,unlink',
					'width' => '100%',
					'height' => '400px'
				);
			}
			// uniquily identify this editors posted data among other (similar) editors
			$this->htmlName = SectionManager::generateHtmlName($section, $this, 'text', $htmlName);
			$this->htmlId = SectionManager::generateHtmlId($section, $this, 'text');
			
			$this->wpEditorOptions = array(
					'teeny' => true,
					'textarea_name' => $this->htmlName,
					'media_buttons'=> $allowMedia,
					'tinymce' => $this->settings
			);
		}
		function htmlName($property){
			$editorName = self::EDITOR_NAME;
			return "[{$editorName}][{$property}]";
		}
		function htmlId(){
			return self::EDITOR_NAME;
		}
		function embed(){
			ob_start();
			wp_editor($this->section->content(), $this->htmlId, $this->wpEditorOptions);
			return ob_get_clean();
		}
		/**
		 * TODO: not yet implemented
		 * (non-PHPdoc)
		 * @see \net\supervillainhq\wordpresscms\admin\content\SectionEditor::sanitize()
		 */
		function sanitize(array $postData){
			return $postData;
		}
		/**
		 * Updates the internal section instance and persists its data
		 * (non-PHPdoc)
		 * @see \net\supervillainhq\wordpresscms\admin\content\SectionEditor::update()
		 */
		function update(array $properties){
			$data = array();
			foreach ($properties as $property => $value){
				switch($property){
					case 'text':
						$this->section->content($value);
						$data['content'] = $value;
						break;
				}
			}
			$data = (object) $this->sanitize($data);
			SectionQuery::update($this->section, $data);
		}
	}
}