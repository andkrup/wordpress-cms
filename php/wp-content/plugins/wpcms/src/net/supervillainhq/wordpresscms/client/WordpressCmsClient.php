<?php
namespace net\supervillainhq\wordpresscms\client{
	use net\supervillainhq\wordpresscms\content\Page;
	use net\supervillainhq\wordpresscms\WordpressCMS;
use net\supervillainhq\wordpresscms\content\Section;
			
	/**
	 * Fetches and prints content in different formats
	 * 
	 * @author ak
	 *
	 */
	interface WordpressCmsClient{
		// wordpress actions
// 		function onPluginsLoaded();
		function onSetupTheme();
// 		function onWP($wp);
		function onWpHead();
		function onInit();
		function onTemplateRedirect();
		// receive the page object used in the current request
		function loadPage(Page $page);
		function wpcms(WordpressCMS $cms);
		function getSectionManager(Section $section);
	}
}