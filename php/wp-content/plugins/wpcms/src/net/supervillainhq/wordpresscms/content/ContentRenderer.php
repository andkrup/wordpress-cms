<?php
namespace net\supervillainhq\wordpresscms\content{
	interface ContentRenderer{
		function content($content = null);
		function render();
	}
}