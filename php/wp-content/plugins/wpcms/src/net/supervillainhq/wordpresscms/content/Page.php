<?php
namespace net\supervillainhq\wordpresscms\content{

	use Phalcon\Mvc\Model;

	class Page extends Model{
		private static $current;

		public $id;
		public $layout; // a tree-structure used to print tags in html (or objects in json/xml) in front-end
		public $contentBuffering;
		
		static function current(){
			return self::$current;
		}

		public function initialize(){
			$this->hasMany('layoutId', 'net\supervillainhq\wordpresscms\content\Layout', 'id', array(
				'alias' => 'layout'
			));
		}
		
		function id(){
			return $this->id;
		}
		function layout($value = null){
			if(is_null($value)){
				return $this->layout;
			}
			$this->layout = $value;
		}
		function contentBuffering($value = null){
			switch($value){
				case 'template_redirect':
				case 'template':
				case 'the_post':
					$this->contentBuffering = $value;
					break;
				default:
					return $this->contentBuffering;
			}
		}
		


		function content($content = null){}
	}
}