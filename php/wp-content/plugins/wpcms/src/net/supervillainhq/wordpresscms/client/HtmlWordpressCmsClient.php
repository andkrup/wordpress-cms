<?php
namespace net\supervillainhq\wordpresscms\client{
	use net\supervillainhq\wordpresscms\content\Page;
	use net\supervillainhq\wordpresscms\content\HtmlSection;
use net\supervillainhq\wordpresscms\admin\content\SectionManager;
		
	class HtmlWordpressCmsClient extends BasicWordpressCmsClient implements WordpressCmsClient{
		private $populatedLayout;
		private $snippets;
		
		function __construct(){
			parent::__construct();
			$this->resetSnippets();
		}
		
// 		function onWP($wp){
// 		}
		
		function loadPage(Page $page){
			parent::loadPage($page);
			// fetch the layout structure and begin translating it to html
			// TODO: we need to find out if the template has hardcoded the layout or if we should get it from datastore
// 			$layout = $page->layout();
// 			$sections = $page->sections();
// 			if(isset($layout)){
// 				$this->populatedLayout = $this->populateLayout($layout, $sections);
// 			}
		}
		
		/**
		 * Reset and populate output buffer
		 * @see \net\supervillainhq\wordpresscms\client\BasicWordpressCmsClient::onTemplateRedirect()
		 */
		function onTemplateRedirect(){
			$this->clearBuffer();
			// determine if developer will be buffering manually in the page template or in here
			if(isset($this->currentpage) && $this->currentpage->contentBuffering() == 'template_redirect'){
				$this->bufferStructuredContent();
			}
		}
		/**
		 * Print the outputbuffer with formatted contents
		 */
		function printStructuredContent($key = 'default', $flush = true){
			// print out buffered contents
			echo $this->fetchBuffer($key, $flush);
		}
		/**
		 * Buffer the layout tree from the root or a selected branch of
		 * the layout tree.
		 * 
		 * @param string $branch
		 */
// 		function bufferStructuredContent($branchName = null){
// 			// buffer contents
// 			if(isset($this->populatedLayout)){
// 				$array = $this->populatedLayout;
// 				if(!is_null($branchName)){
// 					// find the branch that should be buffered (will be removed from tree)
// 					$array = $this->sliceBranch($this->populatedLayout, $branchName);
// 				}
// 				if(isset($array)){
// 					$contents = $this->layoutContent($array);
// 					if(is_null($branchName)){
// 						$this->appendBuffer($contents);
// 					}
// 					else{
// 						$this->appendBuffer($contents, $branchName);
// 					}
// 				}
// 			}
// 		}
		
		/**
		 * Domain-specific transformation of the populated layout tree into html tags.
		 * 
		 * @param array $tree The populated layout structure
		 * @return string The finished html.
		 */
// 		function layoutContent($tree){
// 			$str = "";
// 			foreach ($tree as $key => $item){
// 				$subContents;
// 				$elementId;
// 				$section = $item;
// 				if(is_array($item)){
// 					$elementId = esc_attr($key);
// 					$section = $this->currentpage->getSectionByName($elementId);
// 					$subContents = $this->layoutContent($item);
// 				}
				
// 				$startTag = 'div';
// 				$endTag = '/div';
// 				$cssStyles = '';
// 				$cssClasses = '';
// 				if($section instanceof HtmlSection){
// 					$startTag = $section->startTag();
// 					$endTag = $section->endTag();
// 					$cssStyles = $section->cssStyleString();
// 					if(strlen($cssStyles)>0){
// 						$cssStyles = " styles=\"{$cssStyles}\"";
// 					}
// 					$cssClasses = $section->cssClassString();
// 					if(strlen($cssClasses)>0){
// 						$cssClasses = " class=\"{$cssClasses}\"";
// 					}
// 				}
				
// 				if(isset($subContents)){
// 					$elementStart = "<{$startTag} id=\"{$elementId}\"{$cssClasses}{$cssStyles}>";
// 					$elementEnd = "</{$endTag}>";
// 					$str .= "{$elementStart}\n{$subContents}{$elementEnd}\n";
// 				}
// 				else{
// 					$elementId = esc_attr($section->name());
// 					$str .= "<{$startTag} id=\"{$elementId}\"{$cssClasses}{$cssStyles}></{$endTag}>\n";
// 				}
// 			}
// 			return $str;
// 		}
		
		function onPrintScriptLiterals($contexts = array()){
			if(count($this->snippets)>0){
				echo "<script type=\"text/javascript\">\n";
				foreach ($this->snippets as $context=>$snippets){
					echo "// context: $context\n";
					if(is_array($snippets) && in_array($context, $contexts)){
						$l = count($snippets);
						for($i = 0; $i < $l; $i++){
							$snippet = $snippets[$i];
							if(strlen($snippet)>0){
								echo "$snippet\n";
							}
						}
					}
				}
				echo "</script>\n";
			}
		}
		
		function resetSnippets(){
			$this->snippets = array();
		}
		/**
		* Overwrites previously added snippet with the same priority, if priority is
		* not zero
		*
		* @param unknown $context
		* @param unknown $snippet
		* @param number $priority
		*/
		function addScript($context, $snippet, $priority = 0){
			if(!array_key_exists($context, $this->snippets)){
				$this->snippets[$context] = array($snippet);
				return;
			}
			$array = $this->snippets[$context];
			if($priority === 0){
				array_push($array, array($snippet));
			}
			else{
				if(is_array($array[$priority])){
					array_push($array[$priority], $snippet);
				}
				else{
					$array[$priority] = array($snippet);
				}
			}
			$this->snippets[$context] = $array;
		}
		
		function removeScript($snippet){
			$l = count($this->snippets);
			for ($i = 0; $i < $l; $i++){
				if($this->snippets[$i] == $snippet){
					array_splice($this->snippets, $i, 1);
					return;
				}
			}
		}
	}
}