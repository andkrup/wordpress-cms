<?php
namespace net\supervillainhq\wordpresscms\content{
	interface RenderableSection{
		function contentRenderer(ContentRenderer $renderer = null);
	}
}