<?php
namespace net\supervillainhq\wordpresscms\client{
	use net\supervillainhq\wordpresscms\content\Page;
	use net\supervillainhq\wordpresscms\WordpressCMS;
use net\supervillainhq\wordpresscms\content\Section;
			
	class BasicWordpressCmsClient implements WordpressCmsClient{
		protected $wpcms;
		protected $useAjax;
		protected $currentpage;
		protected $output;
		
// 		function currentPage(){
// 			return $this->currentpage;
// 		}
		
		function __construct(){
			$defaults = array('use_ajax'=>true);
			$options = (object) get_option('wpcmscl_options', $defaults);
			$this->useAjax = $options->use_ajax;
			
// 			add_action('wp', array($this, 'onWP'));
			add_action('template_redirect', array($this, 'onTemplateRedirect'));
		}
		
		function onEnqueueScripts(){
			// add ajax functionality.
			if($this->useAjax){
				wp_register_script('wordpresscms-ajax.js', get_template_directory_uri() . '/js/ajax.js', array('jquery'));
				wp_enqueue_script('wordpresscms-ajax.js');
			}
		}
		function onSetupTheme(){
		}
// 		function onWP($wp){
// 			/**
// 			 * DEFAULTS. TODO: You should be able to discard these lines in a child-theme
// 			 */
// 			if(is_admin()){
// 				return;
// 			}
// 			global $post;
// 			// pass some page info to the front-end
// 			$clInfo = "var pageId = " .$post->ID . ";
// var sectionId = null;\n";
// 			// add the ajaxurl to the front-end when not in admin panel
// 		}
		function onTemplateRedirect(){
		}
		function onWpHead(){
		}
		function onInit(){
		}
		function wpcms(WordpressCMS $cms){
			$this->wpcms = $cms;
		}
		
		function loadPage(Page $page){
			$this->currentpage = $page;
		}

		protected function clearBuffer(){
			$this->output = array();
		}
		protected function appendBuffer($value, $key = 'default'){
			if(!isset($this->output[$key])){
				$this->output[$key] = '';
			}
			$this->output[$key] .= $value;
		}
		protected function prependBuffer($value, $key = 'default'){
			if(!isset($this->output[$key])){
				$this->output[$key] = '';
			}
			$this->output[$key] = "{$value}{$this->output[$key]}";
		}
		public function fetchBuffer($key = 'default', $flush = true){
			if(!isset($this->output[$key])){
				return '';
			}
			$str = $this->output[$key];
			if($flush){
				unset($this->output[$key]);
			}
			return $str;
		}
		
		function onGetPage($pageId = null){
			echo json_encode((object) array('message' => 'not yet implemented'));
			die();
		}
		
		function onGetSection($pageId = null, $sectionId = null){
			echo json_encode((object) array('message' => 'not yet implemented'));
			die();
		}
		
		function getSectionManager(Section $section){
			
		}
	}
}