<?php
/**
 * Created by PhpStorm.
 * User: anders
 * Date: 26/07/16
 * Time: 20:16
 */

namespace net\supervillainhq\wordpresscms\controllers {

	use net\supervillainhq\Injectioning;
	use Phalcon\Di\InjectionAwareInterface;
	use Phalcon\DiInterface;

	class AdminPageController implements WordPressController, InjectionAwareInterface{

		use Injectioning;

		/**
		 * @var
		 */
		private $request;

		function __construct(DiInterface $dependencyInjector, $request){
			$this->setDI($dependencyInjector);
			$this->request = $request;
		}

		function indexAction(){
			$viewVars = ['action' => 'index'];
			echo $this->getDI()->getView()->render('admin/wpcms/page/index', $viewVars);
		}

		function createAction(){
			$viewVars = ['action' => 'create'];
			echo $this->getDI()->getView()->render('admin/wpcms/page/create', $viewVars);
		}

		function editAction($id){
			$viewVars = ['action' => 'edit'];
			echo $this->getDI()->getView()->render('admin/wpcms/page/edit', $viewVars);
		}
	}
}


