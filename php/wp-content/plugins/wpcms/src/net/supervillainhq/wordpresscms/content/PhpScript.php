<?php
namespace net\supervillainhq\wordpresscms\content{
	class PhpScript extends Section{
		private $path;
		private $buffer;
		private $verbose;
		
		function fetch($mode){
			global $wpdb;
			$sql = "select path
			from cms_sections s
			left join cms_phpcontent pc on pc.id = s.content_id and pc.section_id = s.id
			where s.id = %d";
			$this->path = $wpdb->get_var($wpdb->prepare($sql, $this->id));
			switch ($mode){
				case 'edit':
				case 'plain':
					break;
				case 'html':
					// TODO: wee need to implement some minimal security when including arbitrary scripts
					if(is_readable($this->path)){
						ob_start();
						include $this->path;
						$this->buffer = ob_get_clean();
					}
					else{
						if($this->verbose){
							$this->buffer = "[missing file: $this->path]";
						}
					}
					break;
			}
		}
		
		static public function onRegisterPhpScriptMetaboxes(){
		}
		
		static function create($postId){
			global $wpdb;
			$wpdb->query($wpdb->prepare("insert into cms_sections(post_id, type_id) values(%d, 3) on duplicate key update id = LAST_INSERT_ID(id);", $postId));
			return new PhpScript($wpdb->insert_id, $postId, new SectionType(3));
		}
		
		/**
		 * the PhpScript post type
		 * 1-1 relation to a wp_posts row
		 */
		static function register(){
			$labels = array('name'=> "PhpScripts", 'singular_name' => "PhpScript", 'add_new' => "New PhpScript");
			$properties = array('label' => "PhpScript",
					'labels' => $labels,
					'public' => false,
					'has_archive' => true,
					'description' => "included php script",
					'exclude_from_search' => false,
					'show_ui' => true,
					'show_in_menu' => false,
					'show_in_nav_menus' => true,
					'register_meta_box_cb' => array('\\net\\supervillainhq\\wordpresscms\\content\\PhpScript', 'onRegisterPhpScriptMetaboxes'),
					'supports' => array('title')
			);
			register_post_type('phpscript', $properties);
		}
		
		static function onAddPhpScriptMetaBoxes($post){
			$phpscriptmanager = PhpScriptManager::instance();
			$current = Section::load(intval($post->ID));
			if(!isset($current)){
				// create new
				$current = new PhpScript(intval($post->ID));
			}
			$phpscriptmanager->setCurrent($current);
			add_meta_box('phpscript-metabox', 'Basic information', array($phpscriptmanager, 'displayMainPhpScriptMetabox'), 'phpscript', 'normal', 'high');
		}
	}
}