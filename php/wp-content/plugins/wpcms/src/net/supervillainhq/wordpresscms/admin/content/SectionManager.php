<?php
namespace net\supervillainhq\wordpresscms\admin\content{
	use net\supervillainhq\wordpresscms\content\Section;
	use net\supervillainhq\wordpresscms\admin\ContentEditor;
	use net\supervillainhq\wordpresscms\dao\SectionQuery;
use net\supervillainhq\wordpresscms\dao\SectionTypeQuery;
				
	class SectionManager extends Section{
		private $section;
		private $editor;
		
		public function id(){
			return $this->section->id;
		}
		public function name($value = null){
			if(is_null($value)){
				return $this->section->name;
			}
		}
		public function section(Section $section = null){
			if(is_null($section)){
				return $this->section;
			}
			$this->section = $section;
		}
		public function editor(ContentEditor $editor = null){
			if(is_null($editor)){
				return $this->editor;
			}
			$this->editor = $editor;
		}
		
		function __construct(Section $section, SectionEditor $editor = null){
			$this->section = $section;
			$this->editor = $editor;
		}

		static function getSectionEditorByName($editorName, $sectionId){
			switch ($editorName){
				case TinyMceSectionEditor::EDITOR_NAME:
					$section = SectionQuery::restore($sectionId);
					return new TinyMceSectionEditor($section);
			}
			return null;
		}

		static function generateHtmlName(Section $section, SectionEditor $editor, $property, $htmlName = null){
			if(is_null($htmlName)){
				return "sectiondata[{$section->id()}]{$editor->htmlName($property)}";
			}
			return "sectiondata[{$section->id()}][{$htmlName}]";
		}
		static function generateHtmlId(Section $section, SectionEditor $editor, $property){
			return "sectiondata_{$section->id()}_{$editor->htmlId()}_{$section->name()}_{$property}";
		}

		static function onAddSectionMetaBoxes(\WP_Post $post){
			$current = SectionQuery::restore(intval($post->ID));
			if(!isset($current)){
				// if not set, we're in the "create-new-section" flow, so we need to create a new instance
				$current = SectionQuery::create(intval($post->ID));
			}
			$sectionmanager = new SectionManager($current);
		
			add_meta_box('section-metabox', 'Basic information', array($sectionmanager, 'displayMainSectionMetabox'), 'section', 'normal');
			add_meta_box('sectioncontents-metabox', 'Contents', array($sectionmanager, 'displaySectionContentsMetabox'), 'section', 'normal');
			add_meta_box('sectiontype-metabox', 'Section Type', array($sectionmanager, 'displaySectionTypeMetabox'), 'section', 'side');
		}
		
		static function onSaveSection($postId){
// 			var_dump($_POST);
			if(isset($_POST['section'])){
				$sectionData = $_POST['section'];
				if(isset($sectionData[$postId])){
					$sectionInstanceData = (object) $sectionData[$postId];
					$section = SectionQuery::restore($postId);
					if(!isset($section)){
						throw new \Exception('section not set');
					}
					SectionQuery::update($section, $sectionInstanceData);
				}
			}
		}
		
		function displayMainSectionMetabox(){
			echo "displayMainSectionMetabox()";
		}
		function displaySectionContentsMetabox(){
			$editorId = "editor_{$this->section->id}";
			$editorName = "section[{$this->section->id}][content]";
			$tinyMceSettings = array(
// 					'toolbar1'=> get_option('ddai-tinymce-toolbar1'),
					'width' => '100%',
					'height' => '340px'
			);
			$options = array(
					'teeny' => true,
					'textarea_name' => $editorName,
					'media_buttons'=>false,
					'tinymce' => $tinyMceSettings
			);
			wp_editor($this->section->content(), $editorId, $options);
		}
		function displaySectionTypeMetabox(){
			include ABSPATH."/wp-content/plugins/wordpress-cms/inc/metabox.section.type.php";
		}
		private function listSectionTypes(){
			return SectionTypeQuery::getSectionTypes();
		}
	}
}