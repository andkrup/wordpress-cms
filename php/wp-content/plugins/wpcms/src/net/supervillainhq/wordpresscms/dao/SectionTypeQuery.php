<?php
namespace net\supervillainhq\wordpresscms\dao{
	use net\supervillainhq\wordpresscms\content\SectionType;
	class SectionTypeQuery{
		static function getSectionTypes(){
			global $wpdb;
			$sql = "select id, uname, description, wp_cms_classname
					from cms_SectionTypes;";
			$rows = $wpdb->get_results($sql);
			$types = array();
			if(isset($rows)){
				foreach ($rows as $row){
					$type = new SectionType($row->id);
					$type->name($row->uname);
					$type->description($row->description);
					$type->classPath($row->wp_cms_classname);
					array_push($types, $type);
				}
			}
			return $types;
		}
		static function restore($id){
// 			global $wpdb;
// 			$sql = "select s.id as section_id, instance_name, p.post_content from cms_Sections s
// 					left join wp_posts p on p.ID = s.id
// 					where s.id = %d";
// 			$row = $wpdb->get_row($wpdb->prepare($sql, $id));
// 			if(isset($row)){
// 				$section = new Section($id);
// 				$section->name($row->instance_name);
// 				$section->content($row->post_content);
// 				return $section;
// 			}
			return null;
		}
		
	}
}