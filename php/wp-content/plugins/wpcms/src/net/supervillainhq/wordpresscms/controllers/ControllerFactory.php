<?php
/**
 * Created by PhpStorm.
 * User: anders
 * Date: 26/07/16
 * Time: 20:21
 */

namespace net\supervillainhq\wordpresscms\controllers {
	interface ControllerFactory{
		function getController();
	}
}


