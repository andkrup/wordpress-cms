<?php
namespace net\supervillainhq\wordpresscms\content{
	class SectionType{
		protected $id;
		protected $name;
		protected $description;
		protected $classPath;
		
		public function __construct($id){
			$this->id = $id;
		}
		
		public function id(){
			return $this->id;
		}
		public function name($name = null){
			if(!is_null($name)){
				$this->name = $name;
			}
			return $this->name;
		}
		public function description($description = null){
			if(is_null($description)){
				return $this->description;
			}
			$this->description = $description;
		}
		public function classPath($classPath = null){
			if(is_null($classPath)){
				return $this->classPath;
			}
			$this->classPath = $classPath;
		}
		
		
	}
}
