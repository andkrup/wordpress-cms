<?php
namespace net\supervillainhq\wordpresscms\admin\content{
	use net\supervillainhq\wordpresscms\admin\ContentEditor;
	
	class HtmlSectionEditor extends SectionEditor implements ContentEditor{
		const EDITOR_NAME = 'html_editor';
		
		function htmlName($property){
			$name = self::EDITOR_NAME;
			return "[{$name}][{$property}]";
		}
		function htmlId(){
			return self::EDITOR_NAME;
		}
		function embed(){
			return "embedded-editor-html";
		}
		function sanitize(array $postData){
			return $postData;
		}
		function update(array $properties){}
	}
}