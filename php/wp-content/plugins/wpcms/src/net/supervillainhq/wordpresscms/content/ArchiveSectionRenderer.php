<?php
namespace net\supervillainhq\wordpresscms\content{
	
	class ArchiveSectionRenderer implements ContentRenderer{
		protected $posts;
		
		function content($content = null){
			if(is_null($content)){
				return $this->posts;
			}
			$this->posts = $content;
		}
		function render(){
			$return = '';
			if(isset($this->posts)){
				foreach ($this->posts as $post){
// 					$return .= "[POST[{$post->ID}]]";
					$content = apply_filters('get_the_excerpt', $post->post_excerpt);
					$content = apply_filters('the_excerpt', $content);
					$return .= "<h3>{$post->post_title}</h3>\n{$content}";
				}
			}
			return $return;
		}
	}
}