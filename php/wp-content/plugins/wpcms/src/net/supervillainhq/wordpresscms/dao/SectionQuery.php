<?php
namespace net\supervillainhq\wordpresscms\dao{
	use net\supervillainhq\wordpresscms\content\Section;
	
	class SectionQuery{
		protected $section;
		function __construct(Section $section = null){
			if(isset($section)){
				$this->section = $section;
			}
		}
		static function create($postId){
			global $wpdb;
			$sql = "insert into cms_Sections (id)
					values (%d);";
			$wpdb->query($wpdb->prepare($sql, $postId));
			$section = new Section($postId);
			return $section;
		}
		static function restore($id){
			global $wpdb;
			$sql = "select s.id as section_id, instance_name, p.post_content from cms_Sections s
					left join wp_posts p on p.ID = s.id
					where s.id = %d";
			$row = $wpdb->get_row($wpdb->prepare($sql, $id));
			if(isset($row)){
				$section = new Section($id);
				$section->name($row->instance_name);
				$section->content($row->post_content);
				return $section;
			}
			return null;
		}
		static function update(Section $section, $data = null){
			$id = $section->id();
			$post = get_post($id);
			global $wpdb;
			$wpdb->update($wpdb->posts, array('post_content' => $data->content), array('ID' => $id));
		}
		function delete(){}
		function find(){}
	}
}