<?php
namespace net\supervillainhq\wordpresscms{

	use net\supervillainhq\EventManaging;
	use net\supervillainhq\Injectioning;
	use net\supervillainhq\wordpresscms\client\BasicWordpressCmsClient;
	use net\supervillainhq\wordpresscms\client\WordpressCmsClient;
	use net\supervillainhq\wordpresscms\admin\WordpressCmsAdminClient;
	use Phalcon\DiInterface;

	class WordpressCMSPlugin{
		use Injectioning;
		use EventManaging;

		private static $instance;
//		private static $pagePointers = array();
		private $wpcms; // backend
		private $wpcmsAdmin; // Administration connections to the cms backend
		private $templateClient; // Normal connections to the cms backend
		private $clients; // List of content presenters
		private $populatedLayout;
		
		static public function instance(DiInterface $dependencyInjector){
			if(!isset(self::$instance)){
				self::$instance = new WordpressCMSPlugin($dependencyInjector);
			}
			return self::$instance;
		}
		
		function admin(WordpressCmsAdminClient $value = null){
			if(is_null($value)){
				return $this->wpcmsAdmin;
			}
			$this->wpcmsAdmin = $value;
			$this->registerClient($value);
		}
		function templateClient(WordpressCmsClient $value = null){
			if(is_null($value)){
				return $this->templateClient;
			}
			$this->templateClient = $value;
			$this->registerClient($value);
		}
		
		private function __construct(DiInterface $dependencyInjector){
			$this->setDI($dependencyInjector);
			$this->wpcms = WordpressCMS::instance();
			$this->resetClients();
//			add_action('plugins_loaded', array($this, 'onPluginsLoaded')); // After active plugins and pluggable functions are loaded
			add_action('init', array($this, 'onInit')); // Typically used by plugins to initialize. The current user is already authenticated by this time.
			add_action('wp_loaded', array($this, 'onWpLoaded')); // After WordPress is fully loaded
			add_action('wp', array($this, 'onWP')); // After WP object is set up (ref array)
			if(is_admin()){
				add_action('current_screen', array($this, 'onAdminScreen'));
				add_action('admin_init', array($this, 'onAdminInit'));
				add_action('admin_menu', array($this, 'onAdminMenu'));
				add_action('load-post.php', array($this, 'onAdminLoadPost'));
//				add_action('admin_enqueue_scripts', array($this, 'onAdminEnqueueScripts'));
				// ajax endpoint actions
				add_action('wp_ajax_foobar', array($this, 'onAjax'));
				add_action('wp_ajax_get_editor', array($this, 'onAjaxGetEditor'));
 				add_action('add_meta_boxes_cmspage', array($this, 'onAddCmsPageMetaBoxes'), 10, 1);
				add_action('add_meta_boxes_section', array($this, 'onAddSectionMetaBoxes'), 10, 1);
// 				add_action('add_meta_boxes_phpscript', array('net\\supervillainhq\\wordpresscms\\content\\PhpScript', 'onAddPhpScriptMetaBoxes'), 10, 1);
//				add_action('add_meta_boxes_page', array($this, 'onAddPageMetaBoxes'), 10, 1);
			}
		}
		
		static function setPluginHooks($path){
			register_activation_hook($path, array("net\\supervillainhq\\wordpresscms\\WordpressCMSPlugin", 'onInstall'));
			register_deactivation_hook($path, array("net\\supervillainhq\\wordpresscms\\WordpressCMSPlugin", 'onDeactivate'));
			register_uninstall_hook($path, array("net\\supervillainhq\\wordpresscms\\WordpressCMSPlugin", 'onUninstall'));
		}
		

		function onInit(){
		}
		
		function onWpLoaded(){
			// global $post is null at this point;
		}
		
		function onAdminScreen(){
		}
		
		function onAdminInit(){
			// When managing the admin panel, we need an adminclient
			if(is_admin()){
				$this->admin(new WordpressCmsAdminClient());
			}

		}
		
		function onAdminMenu(){
			// bootstrap pseudo-routes to admin pages
			add_menu_page('Pages', 'Pages', 'manage_options', 'cms/pages', function(){
				return $this->delegateToController('cms/pages');
			});
			add_submenu_page('cms/pages', 'Create Page', 'Create Page', 'publish_posts', 'cms/pages/create', function(){
				return $this->delegateToController('cms/pages/create');
			});
			add_submenu_page(null, 'Edit Page', 'Edit Page', 'publish_posts', 'cms/pages/edit', function(){
				return $this->delegateToController('cms/pages/edit');
			});
			add_submenu_page('wordpress-cms/pages.php', 'Sections', 'Sections', 'publish_posts', 'wordpress-cms/pages/sections.php');
			add_menu_page('Layouts', 'Layouts', 'manage_options', 'cms/layouts', function(){
				return $this->delegateToController('cms/layouts');
			});
			add_menu_page('Styles', 'Styles', 'manage_options', 'cms/styles', function(){
				return $this->delegateToController('cms/styles');
			});
//			add_menu_page('Rows', 'Rows', 'manage_options', 'wordpress-cms/page-rows.php', [$this, 'viewRows']);
			add_menu_page('Content Managing', 'CMS', 'manage_options', 'cms/main', function(){
				return $this->delegateToController('cms');
			});
			add_submenu_page('wordpress-cms/main.php', 'Countries', 'Countries', 'publish_posts', 'wordpress-cms/main/countries.php');
			add_submenu_page('wordpress-cms/main.php', 'Locales', 'Locales', 'publish_posts', 'wordpress-cms/main/locales.php');
			add_submenu_page('wordpress-cms/main.php', 'Languages', 'Languages', 'publish_posts', 'wordpress-cms/main/languages.php');
			add_submenu_page('wordpress-cms/main.php', 'Php-scripts', 'Php-scripts', 'publish_posts', 'wordpress-cms/scripts.php');
		}

		private function delegateToController($request, array $args = []){
			$this->getDI()->getController($request, $args);
		}

		function onAdminLoadPost(){
		}
		
		/**
		 * Triggered after WP is loaded
		 * @param unknown $wp
		 */
		public function onWP($wp){
			// If we're not using themes, we won't be able to instantiate  a client in the theme functions.php (or elsewhere in the theme)
			if (defined('WP_USE_THEMES') && !WP_USE_THEMES){
				// so we instantiate a default basic client
				$this->registerClient(new BasicWordpressCmsClient());
			}

		}




		function getLayoutBranch($branch){
			return $this->sliceBranch($this->populatedLayout, $branch);
		}
		


		
		function onAjax(){
			$buffer = (object) array(
				'posted'=> (object) $_POST
			);
//			Output::printJson($buffer);
			switch ($_POST['action']){
				case 'get_editor':
					break;
			}
		}
		
		function onAjaxGetEditor(){
			$editor = $this->wpcms->getEditor(trim($_POST['editor']));
			ob_start();
			wp_editor($editor->text, $editor->name, $editor->settings);
			$html = ob_get_clean();
			$buffer = (object) array(
					'html' => $html,
					'code'=>0
			);
//			Output::printJson($buffer);
		}

 		static function onAddCmsPageMetaBoxes(){
// 			add_meta_box('section-metabox', 'Basic information', array($instance, 'displayMainSectionMetabox'), 'section', 'normal');
// 			add_meta_box('sectiontype-metabox', 'Section Type', array($this, 'displaySectionTypeMetabox'), 'section', 'side');
 		}

		
		function resetClients(){
			if(isset($this->clients)){
				foreach ($this->clients as $client){
					$client->wpcms(null);
				}
			}
			$this->clients = array();
		}
		function registerClient(WordpressCmsClient $client){
			array_push($this->clients, $client);
			$client->wpcms($this->wpcms);
		}
		function hasClient(WordpressCmsClient $client){
			foreach($this->clients as $i => $cl){
				if($cl == $client){
					return true;
				}
			}
			return false;
		}
		function unregisterClient(WordpressCmsClient $client){
			foreach($this->clients as $i => $cl){
				if($cl == $client){
					array_splice($this->clients, $i, 1);
					$client->wpcms(null);
					return true;
				}
			}
			return false;
		}
		
		static function onInstall(){
		}
		static function onDeactivate(){}
		static function onUninstall(){}
		
	}
}
?>