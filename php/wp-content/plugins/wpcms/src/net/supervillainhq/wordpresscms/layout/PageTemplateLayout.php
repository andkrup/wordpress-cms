<?php
namespace net\supervillainhq\wordpresscms\layout{
	interface PageTemplateLayout{
		function restore();
		function update();
		function delete();
		function create();
		function find();
	}
}