<?php
namespace net\supervillainhq\wordpresscms\content{
	use Phalcon\Mvc\Model;

	class Section extends Model implements ContentSection{
		use SectionCollectioning;

		public $id;
		public $name;
		public $content;
		public $locale;

		public function __construct($id = null){
			$this->id = intval($id);
		}
		
		public function id(){
			return $this->id;
		}
		public function name($value = null){
			if(is_null($value)){
				return $this->name;
			}
			$this->name = $value;
		}
		public function content($content = null){
			if(is_null($content)){
				return $this->content;
			}
			$this->content = $content;
		}
	}
}