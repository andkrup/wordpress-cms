<?php
namespace net\supervillainhq\wordpresscms\content{
	interface ContentSection{
		function content($content = null);
	}
}