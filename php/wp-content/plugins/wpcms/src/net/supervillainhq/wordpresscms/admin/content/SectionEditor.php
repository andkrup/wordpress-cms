<?php
namespace net\supervillainhq\wordpresscms\admin\content{
	use net\supervillainhq\wordpresscms\content\Section;
	use net\supervillainhq\wordpresscms\admin\ContentEditor;
		
	class SectionEditor implements ContentEditor{
		protected $section;
		
		function __construct(Section $section){
			$this->section = $section;
		}
		
		function htmlName($property){
			return "[editor][{$property}]";
		}
		function htmlId(){
			return "editor";
		}
		function embed(){
			return "embedded-editor-html";
		}
		function sanitize(array $postData){}
		function update(array $properties){}
	}
}