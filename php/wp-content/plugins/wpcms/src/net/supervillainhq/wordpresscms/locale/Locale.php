<?php
/**
 * Created by PhpStorm.
 * User: anders
 * Date: 25/07/16
 * Time: 23:31
 */

namespace net\supervillainhq\wordpresscms\locale {

	use Phalcon\Mvc\Model;

	class Locale extends Model{
		public $id;
		public $name;
		public $iso2;
		public $iso3;
		public $language;
	}
}


