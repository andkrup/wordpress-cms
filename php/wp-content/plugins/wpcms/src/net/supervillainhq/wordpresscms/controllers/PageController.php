<?php
/**
 * Created by PhpStorm.
 * User: anders
 * Date: 26/07/16
 * Time: 20:08
 */

namespace net\supervillainhq\wordpresscms\controllers {

	use net\supervillainhq\Injectioning;
	use Phalcon\Di\InjectionAwareInterface;
	use Phalcon\DiInterface;

	class PageController implements WordPressController, InjectionAwareInterface{

		use Injectioning;
		/**
		 * @var
		 */
		private $request;

		function __construct(DiInterface $dependencyInjector, $request){
			$this->request = $request;
		}

		function handleAction($action, array $args = []){
			// TODO: Implement handleAction() method.
		}
	}
}


