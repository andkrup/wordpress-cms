<?php
namespace net\supervillainhq\wordpresscms\admin\content{
	use net\supervillainhq\wordpresscms\admin\ContentEditor;
	
	class ArchiveSectionEditor extends SectionEditor implements ContentEditor{
		const EDITOR_NAME = "arch_editor";
		protected $htmlId;
		protected $htmlName;
		
// 		function __construct(Section $section){
// 			parent::__construct($section);
// 		}
		
		function htmlName($property){
			$editorName = self::EDITOR_NAME;
			return "[{$editorName}][{$property}]";
		}
		function htmlId(){
			return self::EDITOR_NAME;
		}
		
		function embed(){
			return "archive_editor";
		}
		function sanitize(array $postData){
			return $postData;
		}
		/**
		 * Updates the internal section instance and persists its data
		 * (non-PHPdoc)
		 * @see \net\supervillainhq\wordpresscms\admin\content\SectionEditor::update()
		 */
		function update(array $properties){
			$data = array();
			foreach ($properties as $property => $value){
				switch($property){
					case 'text':
						$this->section->content($value);
						$data['content'] = $value;
						break;
				}
			}
			$data = (object) $this->sanitize($data);
			SectionQuery::update($this->section, $data);
		}
	}
}