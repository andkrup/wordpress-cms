<?php
/**
 * Created by PhpStorm.
 * User: anders
 * Date: 26/07/16
 * Time: 20:22
 */

namespace net\supervillainhq\wordpresscms\controllers {

	use net\supervillainhq\Injectioning;
	use Phalcon\Di\InjectionAwareInterface;
	use Phalcon\DiInterface;

	class AdminPageControllerFactory implements ControllerFactory, InjectionAwareInterface{

		use Injectioning;

		/**
		 * @var
		 */
		private $request;

		function __construct(DiInterface $dependencyInjector, $request){
			$this->setDI($dependencyInjector);
			$this->request = $request;
		}

		function getController(array $args = []){
			$context = null;
			$controller = null;
			$action = null;
			$parameters = [];
			$frags = explode('/', $this->request);
			switch (count($frags)){
				case 2:
					list($context, $controller) = $frags;
					break;
				case 3:
					list($context, $controller, $action) = $frags;
					break;
				case 4:
					list($context, $controller, $action, $parameters) = $frags;
					break;
			};
			if('cms' == $context){
				switch ($controller){
					case 'pages':
					default:
						if(is_null($action)){
							$action = 'index';
						}
						$controller = new AdminPageController($this->getDI(), $this->request);
						if(!count($parameters)){
							call_user_func([$controller, "{$action}Action"]);
						}
						else{
							call_user_func_array([$controller, "{$action}Action"], $parameters);
						}

						break;
				}
			}
		}
	}
}


