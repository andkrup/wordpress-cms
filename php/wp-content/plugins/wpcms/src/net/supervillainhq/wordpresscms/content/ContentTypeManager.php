<?php
namespace net\supervillainhq\wordpresscms\content{
	class ContentTypeManager{
		public function displayMenu(){
			include dirname(__FILE__)."/../tpl/admin/contenttypes.php";
		}
		public function displayOptions(){
			include dirname(__FILE__)."/../tpl/admin/contenttypes.options.php";
		}
		
		public static function listAll(){
			$sql = "select * from cms_contenttypes;";
			global $wpdb;
			$rows = $wpdb->get_results($sql);
			$ctypes = array();
			foreach ($rows as $row){
				array_push($ctypes, (object) $row);
			}
			return $ctypes;
		}
	}
}
