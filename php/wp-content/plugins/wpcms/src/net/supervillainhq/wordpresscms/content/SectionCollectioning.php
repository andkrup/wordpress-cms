<?php
namespace net\supervillainhq\wordpresscms\content{
	trait SectionCollectioning{
		private $sections;
		
		public function sections(){
			return $this->sections;
		}
		public function clearSections(){
			$this->sections = array();
		}
		public function addSection(Section $section){
			array_push($this->sections, $section);
		}
		public function removeSection(Section $section){
			foreach ($this->sections as $i => $item){
				if($section == $item){
					unset($this->sections[$i]);
					return true;
				}
			}
			return false;
		}
		public function containsSection(Section $section){
			foreach ($this->sections as $i => $item){
				if($section == $item){
					return true;
				}
			}
			return false;
		}
		public function getSectionByName($name){
			if(isset($this->sections)){
				foreach ($this->sections as $section){
					if($section->name() == $name){
						return $section;
					}
				}
			}
			return null;
		}
		
// 		public function __toString(){
// 			$buffer = $this->getAllSectionContents();
// 			return "$buffer";
// 		}
	}
}