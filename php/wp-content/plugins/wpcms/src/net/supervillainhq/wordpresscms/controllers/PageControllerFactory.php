<?php
/**
 * Created by PhpStorm.
 * User: anders
 * Date: 26/07/16
 * Time: 20:22
 */

namespace net\supervillainhq\wordpresscms\controllers {

	use net\supervillainhq\Injectioning;
	use Phalcon\Di\InjectionAwareInterface;
	use Phalcon\DiInterface;

	class PageControllerFactory implements ControllerFactory, InjectionAwareInterface{

		use Injectioning;

		/**
		 * @var
		 */
		private $request;

		function __construct(DiInterface $dependencyInjector, $request){
			$this->setDI($dependencyInjector);
			$this->request = $request;
		}

		function getController(){
			return new PageController($this->getDI(), $this->request);
		}
	}
}


