<?php
namespace net\supervillainhq\wordpresscms\dao{
	use net\supervillainhq\wordpresscms\content\HtmlSection;
	
	class HtmlSectionDAO extends SectionDAO implements DAO{
		function __construct(HtmlSection $section){
			parent::__construct($section);
		}
		function create(){}
		function restore(){}
		function update(){}
		function delete(){}
		function find(){}
		
	}
}