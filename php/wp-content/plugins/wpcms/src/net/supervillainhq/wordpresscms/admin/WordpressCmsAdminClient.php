<?php
namespace net\supervillainhq\wordpresscms\admin{
	use net\supervillainhq\wordpresscms\client\WordpressCmsClient;
	use net\supervillainhq\wordpresscms\client\HtmlWordpressCmsClient;
	use net\supervillainhq\wordpresscms\content\Page;
	use net\supervillainhq\wordpresscms\content\HtmlSection;
	use net\supervillainhq\wordpresscms\admin\content\HtmlSectionManager;
	use net\supervillainhq\wordpresscms\admin\content\SectionManager;
	use net\supervillainhq\wordpresscms\admin\content\TinyMceSectionEditor;
	use net\supervillainhq\wordpresscms\content\ArchiveSection;
	use net\supervillainhq\wordpresscms\admin\content\ArchiveSectionEditor;
								
	class WordpressCmsAdminClient extends HtmlWordpressCmsClient implements WordpressCmsClient{
		private $snippets;
		
		function __construct(){
			parent::__construct();
			$this->resetSnippets();
		}
		
// 		function onWP($wp){
			/**
			 * DEFAULTS. TODO: You should be able to discard these lines in a child-theme
			 */
			// 			global $post;
			// 			// pass some page info to the front-end
			// 			$clInfo = "var pageId = " .$post->ID . ";
			// var sectionId = null;\n";
			// 			// add the ajaxurl to the front-end when not in admin panel
// 		}
		
		function loadPage(Page $page){
			$this->currentpage = $page;
			// fetch the layout structure and begin translating it to section for editing
// 			$layout = $page->layout();
// 			$sections = $page->sections();
// 			if(isset($layout)){
// 				$this->populatedLayout = $this->populateLayout($layout, $sections);
// 			}
		}
		
		function layoutContent($tree){
			$str = "";
			var_dump($tree);
			foreach ($tree as $key => $item){
				$subContents;
				$elementId;
				$section = $item;
				if(is_array($item)){
					$elementId = esc_attr($key);
					$section = $this->currentpage->getSectionByName($elementId);
					$subContents = $this->layoutContent($item);
				}
		
				$cssStyles = '';
				$cssClasses = '';
		
				if(isset($subContents)){
					$elementStart = "<div id=\"{$elementId}\" class=\"column layout\">";
					$elementEnd = "</div>";
					$str .= "{$elementStart}\n{$subContents}{$elementEnd}\n";
				}
				else{
					$elementId = esc_attr($section->name());
					$str .= "<div id=\"{$elementId}\" class=\"row layout\"></div>\n";
				}
			}
			return $str;
		}
		
			
// 		function getEditors(){
// 			var_dump($this->populatedLayout['section'][2]);
// 			$text = "<h3>LIST OF ALL SECTIONS DISPLAYED ON THIS PAGE</h3>
// 				<p>Page layout is handled by the pages child-sections. Each child-section has its
// 				own set of css rules that defines how it positions itself against other child-sections.</p>
// 				<p>Furthermore the order of the child-sections, determines the order in which it
// 				is printed</p>
// 				<h3>Section types</h3>
// 				<p>Each section will be displayed, using its own edit-box, in order to let the
// 				main page metabox be fairly wysisyg, regarding layout</p>
// 				<p>But the first priority is to design how new sections can be added to the currently
// 				edited page</p>";
// 			$baseurl = get_option('siteurl');
// 			$settings = array(
// 					'textarea_name'=>'feature_editor',
// 					'media_buttons'=>true,
// 					'editor_class'=>'col1_content_editor',
// 					'textarea_rows'=>'25',
// 					'tinymce'=> array(
// 							'document_base_url'=>$baseurl,
// 							'relative_urls'=>true,
// 							'height'=>350
// 					)
// 			);
// 			$editor = array(
// 				'name'=>esc_attr('feature'),
// 				'text'=>$text,
// 				'settings'=>$settings
// 			);
// 			return (object) $editor;
// 		}
	}
}