<?php
namespace net\supervillainhq\wordpresscms\admin{
	interface ContentEditor{
		function htmlName($property);
		function htmlId();
		function embed();
		function sanitize(array $postData);
		function update(array $properties);
	}
}