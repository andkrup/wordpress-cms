<?php
namespace net\supervillainhq\wordpresscms\content{
	class HtmlSection extends Section{
		protected $startTag;
		protected $endTag;
		protected $cssStyles;
		protected $cssClasses;
		protected $innerText;
		
		function innerText($value = null){
			if(is_null($value)){
				return $this->innerText;
			}
			$this->innerText = $value;
		}
		
		function __construct($id = null){
			parent::__construct($id);
			$this->resetCssStyles();
			$this->resetCssClasses();
		}
		
		function content($content = null){
			if(!is_null($content)){
				parent::content($content);
			}
			return $this->renderHtml();
		}
		protected function renderHtml(){
			$startTag = $this->startTag();
			$endTag = $this->endTag();
			$cssStyles = $this->cssStyleString();
			if(strlen($cssStyles)>0){
				$cssStyles = " styles=\"{$cssStyles}\"";
			}
			$cssClasses = $this->cssClassString();
			if(strlen($cssClasses)>0){
				$cssClasses = " class=\"{$cssClasses}\"";
			}
			$elementId = esc_attr($this->name());
			$subContents = $this->innerText; // TODO: no check for nested sections
			if(isset($subContents)){
				$elementStart = "<{$startTag} id=\"{$elementId}\"{$cssClasses}{$cssStyles}>";
				$elementEnd = "</{$endTag}>";
				return "{$elementStart}\n{$subContents}{$elementEnd}\n";
			}
			else{
				return "<{$startTag} id=\"{$elementId}\"{$cssClasses}{$cssStyles}></{$endTag}>\n";
			}
			
		}
		
		public function startTag($value = null){
			if(is_null($value)){
				return $this->startTag;
			}
			$this->startTag = $value;
		}
		public function endTag($value = null){
			if(is_null($value)){
				return $this->endTag;
			}
			$this->endTag = $value;
		}
		public function cssStyles(array $value = null){
			if(is_null($value)){
				return $this->cssStyles;
			}
			$this->cssStyles = $value;
		}
		public function cssClasses(array $value = null){
			if(is_null($value)){
				return $this->cssClasses;
			}
			$this->cssClasses = $value;
		}
		
		function resetCssStyles(){
			$this->cssStyles = array();
		}
		function addCssStyle($selector, $cssstyle){
			$this->cssStyles[$selector] = $cssstyle;
		}
		function removeCssStyle($selector){
			unset($this->cssStyles[$selector]);
		}
		function hasCssStyle($selector){
			return array_key_exists($selector, $this->cssStyles);
		}
		function getCssStyle($selector){
			return $this->cssStyles[$selector];
		}
		function cssStyleString($value = null){
			if(is_null($value)){
				$str = '';
				foreach($this->cssStyles as $selector => $setting){
					$str .= "$selector: $setting;";
				}
				return $str;
			}
			$this->resetCssStyles();
			$rules = explode(';', $value);
			foreach ($rules as $rule){
				$frag = explode(':', $rule);
				$selector = $frag[0];
				$setting = $frag[1];
				$this->cssStyles[$selector] = $setting;
			}
		}
		
		function resetCssClasses(){
			$this->cssClasses = array();
		}
		function addCssClass($cssclass){
			array_push($this->cssClasses, $cssclass);
		}
		function removeCssClass($cssclass){
			$str = implode(',', $this->cssClasses);
			$str = str_replace($cssclass, '', $str);
			$this->cssClasses = explode(',', $str);
		}
		function hasCssClass($cssclass){
			return in_array($cssclass, $this->cssClasses);
		}
		function cssClassString($value = null){
			if(is_null($value)){
				return implode(' ', $this->cssClasses);
			}
			$this->cssClasses = explode(' ', $value);
		}
	}
}