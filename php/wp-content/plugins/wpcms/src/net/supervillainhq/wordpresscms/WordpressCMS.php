<?php
namespace net\supervillainhq\wordpresscms{
	use net\supervillainhq\wordpresscms\content\Page;
	use net\supervillainhq\wordpresscms\content\PageManager;
use net\supervillainhq\wordpresscms\content\Section;
use net\supervillainhq\wordpresscms\admin\content\SectionManager;
				
	class WordpressCMS{
		const PHP_SCRIPTS_PATH = 'phpscripts';
		public $pageManager;
		private static $inst;
	
		public static function instance(){
			if(!isset($inst)){
				$inst = new WordpressCMS();
			}
			return $inst;
		}
	
		private function __construct(){
			// run this when activating this theme
			add_action('after_switch_theme', array('net\\supervillainhq\\wordpresscms\\WordpressCMS', 'onActivation'));
			// run this on every REQUEST
			add_action('init', array('net\\supervillainhq\\wordpresscms\\WordpressCMS', 'onInit'));
			add_action('after_setup_theme', array($this, 'onSetupTheme'));
// 			add_action('wp', array($this, 'onWP'));
			if(is_admin()){
				$this->pageManager = new PageManager($this);
				add_action('admin_init', array('net\\supervillainhq\\wordpresscms\\WordpressCMS', 'onAdminInit'));
				add_action('admin_head', array('net\\supervillainhq\\wordpresscms\\WordpressCMS', 'onAdminHead'));
				add_action('admin_menu', array('net\\supervillainhq\\wordpresscms\\WordpressCMS', 'onAdminMenu'));
				add_action('admin_notices', array('net\\supervillainhq\\wordpresscms\\WordpressCMS', 'onAdminNotices'));
				add_action('add_meta_boxes', array('net\\supervillainhq\\wordpresscms\\WordpressCMS', 'onAddMetaBoxes'), 10, 2);
				add_action('save_post', array($this, 'onSavePost'));
				add_action('before_delete_post', array('net\\supervillainhq\\wordpresscms\\WordpressCMS', 'onDeletePost'));
			}
		}

		/**
		 * Do installation stuff
		 */
		static function onActivation(){
			$phpScriptsDir = WP_CONTENT_DIR . '/' . self::PHP_SCRIPTS_PATH;
			// set up database schemas
			// register post types, etc...
			// generate required files, etc...
			if(!is_writable($phpScriptsDir)){
				if(!is_dir($phpScriptsDir)){
					if(!is_writable(WP_CONTENT_DIR)){
						throw new \Exception(WP_CONTENT_DIR.' is not writable, so we cannot support dynamic phpscripts in the folder '.$phpScriptsDir.'. Please change the file permissions for '.WP_CONTENT_DIR.', or create the folder '.$phpScriptsDir.' manually');
					}
					mkdir($phpScriptsDir);
				}
			}
		}
		static function onDeactivation(){
			echo "CMS-DEACTIVATE!";
		}

		static function onAdminNotices(){
			// TODO: check that a user exists with the 'editor' role, in order to avoid having the client use an account with administrator role 
		}
		
		static function onAdminMenu(){
		}

		static function onInit(){
			// prepare for invoking the custom hook for adding inline scripts (NOT script-references to an external file) in the <head> 
			add_action('wp_head', array(self::instance(), 'printScriptliterals'), 1);
		}
		
		/**
		 * Fetch all extra cms data that is related to the supplied Wp_Post instance
		 * 
		 * @param \WP_Post $post
		 */
		function fetchPage($postId){
			$page = Page::restore($postId);
			// return a structure that contains layout and content so that clients can print/encode for the front-end
			return $page;
		}
		
		function printScriptliterals(){
			do_action('wpcms_print_scriptliterals', array('default', 'ajax'));
		}
		
		static function onAdminInit(){
			// TODO: make a switch that only adds the scripts that are needed
			wp_register_style('markup.css', get_template_directory_uri() . '/lib/editors/markup/1.1.14/markitup/skins/markitup/style.css', array());
			wp_register_style('markup-settings.css', get_template_directory_uri() . '/lib/editors/markup/sets/html/style.css', array('markup.css'));
			wp_enqueue_style('markup-settings.css');

			wp_register_script('markup.js', get_template_directory_uri() . '/lib/editors/markup/1.1.14/markitup/jquery.markitup.js', array('jquery'));
			wp_register_script('markup-settings.js', get_template_directory_uri() . '/lib/editors/markup/sets/html/set.js', array('markup.js'));
			wp_enqueue_script('markup-settings.js' );
		}

		static function onAdminHead(){
		}

		public function onSetupTheme(){
		}

// 		public function onWP($wp){
// 		}

		/**
		 * Setup/Fetch data for the metaboxes
		 *
		 * @param String $post_type
		 * @param WP_Post $post
		 */
		static function onAddMetaBoxes($post_type, $post){
		}


		function onSavePost($postId){
			if(!array_key_exists('post_type', $_POST)){
				return;
			}
			switch ($_POST['post_type']){
				case 'page':
					PageManager::onSavePage($postId);
					break;
				case Section::WP_POST_TYPE:
					SectionManager::onSaveSection($postId);
					break;
				case 'phpscript':
// 					PhpScriptManager::onSavePhpScript($postId);
					break;
			}
		}
		
		static function onDeletePost($postId){
			if(array_key_exists('post_type', $_POST)){
				switch ($_POST['post_type']){
					case 'page':
// 						PageManager::deletePage($postId);
						break;
					case 'section':
					case 'phpscript':
// 						SectionManager::deleteSection($postId);
						break;
				}
			}
		}
	}	
}