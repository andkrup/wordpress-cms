<?php
namespace net\supervillainhq\wordpresscms\content{
	use net\supervillainhq\wordpresscms\content\Page;
	use net\supervillainhq\wordpresscms\WordpressCMS;
	use net\supervillainhq\wordpresscms\admin\content\SectionManager;
			
	class PageManager{
		private $current;
		private $wpcms;
		
		
		static function onInit(){
		}
	
		function __construct(WordpressCMS $wpcms){
			$this->wpcms = $wpcms;
		}
			
	
		function current($value = null){
			if(is_null($value)){
				return $this->current;
			}
			$this->current = $value;
		}
		
		static public function onAdminFooter(){
			global $post;
			if(isset($post) && $post->post_type == 'page'){
				include dirname(dirname(__FILE__))."/inc/adminfooter.page.php";
			}
		}
		
		static public function onSavePage($postId){
			// page contents consists of sections that each has an editor. Each editor is responsible for updating what it posts
			if(array_key_exists('sectiondata', $_POST)){
				$sectiondata = $_POST['sectiondata'];
				// break up the posted data into section data, then again into editor data
				foreach ($sectiondata as $sectionId => $postdata){
					foreach ($postdata as $editorId => $data){
						// each editor is then allowed to sanitize and update posted data
						$editor = SectionManager::getSectionEditorByName($editorId, $sectionId);
						$editor->update($data);
					}
				}
			}
		}
		
		public function displayMenu(){
			include dirname(__FILE__)."/../tpl/admin/pages.php";
		}
		public function displayAddNewMenu(){
			include dirname(__FILE__)."/../tpl/admin/page.new.php";
		}
		public function displayContentMetabox(){
			include dirname(__FILE__)."/../tpl/metabox/contentblocks.php";
		}
	}
}
?>