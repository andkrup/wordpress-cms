<?php
namespace net\supervillainhq\wordpresscms\content{
		
	class ArchiveSection extends HtmlSection implements ContentSection, RenderableSection{
		protected $posts;
		protected $renderer;
		
		function __construct($id = null){
			parent::__construct($id);
			
			$args = array(
			);
			$this->posts = get_posts($args);
		}
		
		public function content($content = null){
			if(is_null($content)){
				if(isset($this->renderer)){
					$this->renderer->content($this->posts);
					$this->innerText = $this->renderer->render();
					return parent::content();
				}
			}
			$this->content = $content;
		}
		function contentRenderer(ContentRenderer $renderer = null){
			if(is_null($renderer)){
				return $this->renderer;
			}
			$this->renderer = $renderer;
		}
	}
}