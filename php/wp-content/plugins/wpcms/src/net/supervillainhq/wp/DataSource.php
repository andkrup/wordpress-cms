<?php
namespace net\supervillainhq\wp{
	interface DataSource{
		function execute();
		function data();
	}
}