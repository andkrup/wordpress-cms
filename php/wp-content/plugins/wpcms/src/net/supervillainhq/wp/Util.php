<?php
namespace net\supervillainhq\wp{
	class Util{
		/**
		 * Parses all page templates for occurences of get_pagesection() calls.
		 * If any is found, the page layout is locked by the developer, so that
		 * the client can't change the layout.
		 */
		static function get_pagetemplate_metadata($file){
			if ( file_exists( $file ) && is_file( $file ) ) {
				$template_data = implode( '', file( $file ) );
				if( preg_match('|Template Name:(.*)$|mi', $template_data, $name )){
					return sprintf( __( '%s Page Template' ), _cleanup_header_comment($name[1]) );
				}
			}
		}
	}
}