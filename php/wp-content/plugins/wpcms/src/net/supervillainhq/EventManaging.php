<?php
/**
 * Created by PhpStorm.
 * User: anders
 * Date: 25/07/16
 * Time: 11:19
 */

namespace net\supervillainhq;

use Phalcon\Events\ManagerInterface;

trait EventManaging{

	private $eventsManager;


	public function setEventsManager(ManagerInterface $eventsManager){
		$this->eventsManager = $eventsManager;
	}
	public function getEventsManager(){
		return $this->eventsManager;
	}

}