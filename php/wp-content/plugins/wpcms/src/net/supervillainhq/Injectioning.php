<?php
/**
 * Created by PhpStorm.
 * User: anders
 * Date: 25/07/16
 * Time: 11:19
 */

namespace net\supervillainhq;

use Phalcon\DiInterface;

trait Injectioning{

	private $dependencyInjector;

	public function setDI(DiInterface $dependencyInjector){
		$this->dependencyInjector = $dependencyInjector;
	}
	public function getDI(){
		return $this->dependencyInjector;
	}
}