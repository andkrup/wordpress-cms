<?php
/**
 * Plugin Name: Wordpress CMS Plugin
 * Plugin URI: http://supervillainhq.net
 * Description: This plugin transforms wordpress into a CMS-engine.
 * Additionally, the Wordpress CMS Theme can be used as a test of the functionality
 * supplied by this plugin, or the Wordpress CMS Theme can be used as a parent-
 * theme for enterpricey stuff.
 * 
 * Author: SupervillainHQ
 * Version: 1.0
 * Author URI: http://supervillainhq.net
 *
 */
require_once WP_CONTENT_DIR . '/vendor/autoload.php';

// aset up phalcon stuff
$config = include __DIR__ . "/config/config.php";
include __DIR__ . "/config/loader.php";
include __DIR__ . "/config/services.php";


use net\supervillainhq\wordpresscms\WordpressCMSPlugin;

$cms = WordpressCMSPlugin::instance($di);

/**
 * Registers activation/deactivation/uninstall hooks.
 * Also creates a WordpressCMS instance that acts as a backend
 * and as glue between the wordpress-admin and the CMS.
 */
WordpressCMSPlugin::setPluginHooks(__FILE__);
