<?php
if(!isset($this->section)){
	throw new Exception('section not set');
}
$types = $this->listSectionTypes();
$sectionId = $this->section->id();
$selectHtmlName = "section[{$sectionId}]";
$selectHtmlId = "section_{$sectionId}";
?><div>
	<select id="<?php echo $selectHtmlId?>" name="<?php echo $selectHtmlName?>">
		<option>select ...</option>
		<?php foreach ($types as $type):
				$id = $type->id();
				$name = $type->name();
// 				$selected = 
		?>
			<option value="<?php echo $id?>"><?php echo $name?></option>
		<?php endforeach;?>
	</select>
</div>