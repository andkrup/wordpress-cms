<?php
use net\supervillainhq\wordpresscms\admin\content\SectionManager;
$newRow = '<div class="row layout addnew">
		<img src="/wp-content/plugins/wordpress-cms/images/16x16/table_row_insert.png" title="Add new row">
	</div>';

$editors = array();
$fcontents = '';
$econtents = '';
/**
 * If the theme developer has determined that the current page template layout is coded,
 * we can find a layout guide in the themes admin folder
 */
if($this->pageHasManualLayout($page)){
	/** TODO: move into cmsplugin class
	 *  ... so we load the layout template guide and only allow editing layout inside
	 *  the branches that are marked as dynamic in the template layout guide
	 */
	$filepath = $this->getCurrentPageLayoutTemplate($page);
	if(is_readable($filepath)){
		$layoutTemplateContents = file_get_contents($filepath);
		// find all square-bracket tags for content-replacing
		preg_match_all('/\[([a-z]*)\]/i', $layoutTemplateContents, $matches);
		$branchnames = $matches[1];
		$fcontents = $layoutTemplateContents;
		$econtents = $layoutTemplateContents;
		foreach($branchnames as $branchname){
			$branch = $this->getLayoutBranch($branchname);
			if(isset($branch) && $branch instanceof SectionManager){
				$section = $branch->section();
				$editor = $branch->editor();
				$content = $section->content();
				$src = "[{$branchname}]";
				$fcontents = str_replace($src, $content, $fcontents);
				$content = $editor->embed();
				$econtents = str_replace($src, $content, $econtents);
			}
		}
		$fcontents = "{$fcontents}{$newRow}";
	}
}
/**
 * Otherwise we just display the whole layout tree
 */
else{
	$this->wpcmsAdmin->bufferStructuredContent();
	$fcontents = $this->wpcmsAdmin->fetchBuffer();
}
?>
<div id="editor-tabs">
	<ul class="category-tabs">
		<li><a class="wp-switch-editor switch-normal" href="#edit_layout">Layout</a></li>
		<li><a class="wp-switch-editor switch-retina" href="#edit_content">Content</a></li>
	</ul>
	<div id="edit_layout" class="wp-editor-container">
		<div class="column layout addnew">
			<img src="/wp-content/plugins/wordpress-cms/images/16x16/table_add.png" title="Add new outer column">
		</div>
		<?php echo $fcontents;?>
	</div>
	<div id="edit_content" class="wp-editor-container hidden">
		<div class="column layout addnew">
			<img src="/wp-content/plugins/wordpress-cms/images/16x16/table_add.png" title="Add new outer column">
		</div>
		<div id="feature_edit" class="column edit">
			<?php echo $econtents?>
		</div>
	</div>
</div>
<script type="text/javascript">
jQuery(document).ready( function($){
	$(window).load( function(){
		$("#editor-tabs .hidden").removeClass('hidden');
		$("#editor-tabs").tabs();

		var base = $('#edit_layout');
		var columns = base.find('.column.layout').not('.addnew');
		var c = columns.length;
		var w = 80 / (c);
		console.log('c: ' + c + 'w: ' + w);
		columns.each(function(index, value){
			var column = $(this);
			column.css('min-height', 120);
			column.css('float', 'left');
			column.css('width', Math.round(w) + '%');
		});
		
// 		var rows = base.find('.row.layout').not('.addnew');
// 		rows.each(function(index, value){
// 			var row = $(this);
// 			row.css('width', Math.round(w) + '%');
// 		});
		
		var layoutmanager = new LayoutManager();
		var newColButton = $("div.column.layout.addnew img");
		var newRowButton = $("div.row.layout.addnew img");
		newColButton.click(function(){
			console.log('click');
			layoutmanager.addNewColumn();
			return false;
		});
		newRowButton.click(function(){
			console.log('click');
			layoutmanager.addNewRow();
			return false;
		});
	});
});
</script>