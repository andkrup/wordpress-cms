<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateTableScripts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(){
    	if(!$this->hasTable('wpcms_Scripts')){
			$table = $this->table('wpcms_Scripts', ['id' => false, 'primary_key' => 'id']);
			$table->addColumn('id', 'integer', ['signed' => false, 'identity' => true, 'limit' => MysqlAdapter::INT_REGULAR]);
			$table->addColumn('author', 'biginteger', ['signed' => false, 'limit' => MysqlAdapter::INT_BIG]);
			$table->addColumn('title', 'string', ['limit' => 64]);
			$table->addColumn('created_at', 'datetime', ['null' => true, 'default' => null]);
			$table->addColumn('updated_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP']);
			$table->addForeignKey('author', 'wp_users', 'id', array('delete'=> 'NO_ACTION', 'update'=> 'CASCADE'));
			$table->save();
		}
    }
}
