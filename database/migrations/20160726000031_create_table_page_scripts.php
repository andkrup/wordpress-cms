<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateTablePageScripts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(){
    	if(!$this->hasTable('wpcms_PageScripts')){
			$table = $this->table('wpcms_PageScripts', ['id' => false, 'primary_key' => ['page_id', 'script_id']]);
			$table->addColumn('page_id', 'integer', ['signed' => false, 'limit' => MysqlAdapter::INT_REGULAR]);
			$table->addColumn('script_id', 'integer', ['signed' => false, 'limit' => MysqlAdapter::INT_REGULAR]);
			$table->addForeignKey('page_id', 'wpcms_Pages', 'id', array('delete'=> 'CASCADE', 'update'=> 'CASCADE'));
			$table->addForeignKey('script_id', 'wpcms_Scripts', 'id', array('delete'=> 'CASCADE', 'update'=> 'CASCADE'));
			$table->save();
		}
    }
}
