<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateTableLocales extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(){
    	if(!$this->hasTable('Locales')){
			$table = $this->table('Locales', ['id' => false, 'primary_key' => 'id']);
			$table->addColumn('id', 'integer', ['signed' => false, 'identity' => true, 'limit' => MysqlAdapter::INT_REGULAR]);
			$table->addColumn('title', 'string', ['limit' => 64]);
			$table->addColumn('language_id', 'integer', ['signed' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'null' => true]);
			$table->addColumn('iso2', MysqlAdapter::PHINX_TYPE_CHAR, ['limit' => 2]);
			$table->addColumn('iso3', MysqlAdapter::PHINX_TYPE_CHAR, ['limit' => 3, 'null' => true]);
			$table->addForeignKey('language_id', 'Languages', 'id', array('delete'=> 'NO_ACTION', 'update'=> 'CASCADE'));
			$table->save();
		}
    }
}
