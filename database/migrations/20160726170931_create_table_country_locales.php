<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateTableCountryLocales extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(){
    	if(!$this->hasTable('CountryLocales')){
			$table = $this->table('CountryLocales', ['id' => false, 'primary_key' => ['country_id', 'locale_id']]);
			$table->addColumn('country_id', 'integer', ['signed' => false, 'limit' => MysqlAdapter::INT_REGULAR]);
			$table->addColumn('locale_id', 'integer', ['signed' => false, 'limit' => MysqlAdapter::INT_REGULAR]);
			$table->addColumn('code', 'string', ['limit' => 8]);
			$table->addForeignKey('country_id', 'Countries', 'id', array('delete'=> 'CASCADE', 'update'=> 'CASCADE'));
			$table->addForeignKey('locale_id', 'Locales', 'id', array('delete'=> 'CASCADE', 'update'=> 'CASCADE'));
			$table->save();
		}
    }
}
