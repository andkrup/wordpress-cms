<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateTableCountries extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(){
    	if(!$this->hasTable('Countries')){
			$table = $this->table('Countries', ['id' => false, 'primary_key' => 'id']);
			$table->addColumn('id', 'integer', ['signed' => false, 'identity' => true, 'limit' => MysqlAdapter::INT_REGULAR]);
			$table->addColumn('name', 'string', ['signed' => false, 'limit' => 64]);
			$table->addColumn('local_name', 'string', ['signed' => false, 'limit' => 64, 'null' => true]);
			$table->addColumn('country_code', 'string', ['signed' => false, 'limit' => 6]);
			$table->addColumn('phone_prefix', 'string', ['signed' => false, 'limit' => 6, 'null' => true]);
			$table->save();
		}
    }

    public function up(){
	}
}
