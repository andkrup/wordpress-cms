<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

define('WPDB_DRIVER', 'wpdb_driver_pdo_mysql' );

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'WordpressCMS');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'vagrant');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Y9hEn`Uh2EQf;*JY+PH;!?C_0&N>^lwM*[xOVye5X`L9y<H)98FB<( audab5a6}');
define('SECURE_AUTH_KEY',  '4e-[)f1PMwM+X7M4;Z]6#9b,2+Zw&hJ,Ji+6]~A=-wkpJU;Ymt,+gReV.Gk/b9k_');
define('LOGGED_IN_KEY',    '<5PA=^`7Bg-)>Tn-AdfKF;L%U9bz*HZ~~fgf8$IUM&H%Fg;P&3~/45Pm xycpr*s');
define('NONCE_KEY',        'UCCGnX@})|$wxz^$xoXUuuMV^4;j+g(UeytLa 1ok]]4F<UKyq[:|s2nFB`]0.jk');
define('AUTH_SALT',        'AH@jC|MsZR0ZF3z.a}98JaUM>g|2fBwq(ye^m95W$N1;=AY>3`>kNK`w#(E/VZeZ');
define('SECURE_AUTH_SALT', 'yqn ?T5#j!~i+5yLfHT9|Q-suH},)l`J#K)Q+|(fP%v{~Lx`bq>|9ByQ6r+DGC<l');
define('LOGGED_IN_SALT',   'm(N=R,s@)e.F`~RA+%VDQCCu4AgNN=<pAA>?]z|k((3;,>fZjvt9&EhST7tF{k@;');
define('NONCE_SALT',       'J|J.d9MdrX,LTYQgVZ;kz,|nE1|r@x2N/t]S%vH$7<Rb>.%$lih@`cM|v3}_E!Yc');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

# patch wordpress' symlink-realpath-plugins_url problem by hardcoding paths and urls
//define('WP_PLUGIN_DIR', realpath('/var/www/wp-content/plugins') );
//define('WP_PLUGIN_URL', 'http://localhost:9872/wp-content/plugins');
//define('WP_THEMES_DIR', realpath('/var/www/wp-content/themes') );
//define('WP_THEMES_URL', 'http://localhost:9872/wp-content/themes');
//define('WP_CONTENT_DIR', realpath('/var/www//wp-content') );
//define('WP_CONTENT_URL', 'http://localhost:9872/wp-content');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

define('WP_CONTENT_DIR', '/var/www/wordpresscms/php/wp-content');
//require '/var/www/wordpresscms/php/wp-content/db.php';

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
define('WP_HOME','http://wpcms.dev');
define('WP_SITEURL','http://wpcms.dev');
