#!/usr/bin/env bash


apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list

apt-get update



apt-get install -y curl vim wget git nodejs npm
apt-get install -y php7.0 php7.0-curl php7.0-gd php7.0-intl php7.0-mbstring php7.0-xml php-xdebug php-msgpack

debconf-set-selections <<< 'mysql-server mysql-server/root_password password vagrant'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password vagrant'
apt-get install -y apache2 libapache2-mod-fastcgi mysql-server mongodb-org
apt-get install -y gcc make re2c libpcre3-dev php7.0-dev build-essential php7.0-zip

# enable mod_rewrite
a2enmod rewrite actions fastcgi alias

# set up mongo
echo "[Unit]" >> /etc/systemd/system/mongodb.service
echo "Description=High-performance, schema-free document-oriented database" >> /etc/systemd/system/mongodb.service
echo "After=network.target" >> /etc/systemd/system/mongodb.service
echo "" >> /etc/systemd/system/mongodb.service
echo "[Service]" >> /etc/systemd/system/mongodb.service
echo "User=mongodb" >> /etc/systemd/system/mongodb.service
echo "ExecStart=/usr/bin/mongod --quiet --config /etc/mongod.conf" >> /etc/systemd/system/mongodb.service
echo "" >> /etc/systemd/system/mongodb.service
echo "[Install]" >> /etc/systemd/system/mongodb.service
echo "WantedBy=multi-user.target" >> /etc/systemd/system/mongodb.service
systemctl start mongodb
systemctl enable mongodb

# install composer + dependencies
curl -sS https://getcomposer.org/installer | php -- --install-dir=/home/vagrant
mv /home/vagrant/composer.phar /usr/local/bin/composer
chmod +x /usr/local/bin/composer

composer global require "phalcon/zephir:dev-master"

#composer require "phalcon/devtools" -d /usr/local/bin/
#ln -s /usr/local/bin/vendor/phalcon/devtools/phalcon.php /usr/bin/phalcon

# build phalcon extension for php7
git clone https://github.com/phalcon/cphalcon.git -b 2.1.x --single-branch
cd cphalcon/
~/.composer/vendor/bin/zephir build --backend=ZendEngine3

# set up phalcon extension for php
# extension bin will be installed into /usr/lib/php/20151012/
echo "extension=phalcon.so" >> /etc/php/7.0/mods-available/phalcon.ini
ln -s /etc/php/7.0/mods-available/phalcon.ini /etc/php/7.0/fpm/conf.d/20-phalcon.ini
ln -s /etc/php/7.0/mods-available/phalcon.ini /etc/php/7.0/cli/conf.d/20-phalcon.ini

# fix missing node symlink & install node dependencies
ln -s /usr/bin/nodejs /usr/local/bin/node
npm install -g bower less

# replace default documentroot with wordpress install root
#rm -rf /var/www
#ln -fs /vagrant_html /var/www

# replace /var/log/apache2 with symlink to vagrant folder
#rm -rf /var/log/apache2
#ln -fs /vagrant/.apache /var/log/apache2

# pointing enabled default configuration to the one in our repository
ln -fs /var/www/wordpresscms/vagrant/default.conf /etc/apache2/sites-enabled/000-default.conf
# add our custom php settings
#sudo unlink /etc/php/7.0/fpm/pool.d/www.conf
#ln -fs /var/www/wordpresscms/vagrant/php.ini /etc/php/7.0/fpm/php.ini
ln -fs /var/www/wordpresscms/vagrant/www.conf /etc/php/7.0/fpm/pool.d/www.conf

ln -fsn /var/www/wordpresscms/php/wordpress /var/www/wordpresscms/php/current

ln -sf /var/www/wordpresscms/vagrant/wp-config.php /var/www/wordpresscms/php/current/wp-config.php

# symlink submodules sub-folder to wordpress paths
# symlinking the theme folder in the submodule as a theme folder in the docroot
#ln -fsn /var/www/wordpresscms/php/wp-content /var/www/wordpresscms/php/current/wp-content

#mkdir /home/vagrant/uploads
#cp /vagrant_uploads/* /home/vagrant/uploads/
#chown -R vagrant:www-data /home/vagrant/uploads
#chmod -R g+w /home/vagrant/uploads
#ln -fs /home/vagrant/uploads /var/www/wp-content/uploads

# re-establish database PNO with user 'pno'@'localhost'
#mysql -u root -e "grant all privileges on *.* to 'root'@'%' with grant option;"
#mysql -u root -e "flush privileges;"
#mysql -u root -e "create database if not exists WordpressCMS default character set utf8 default collate utf8_general_ci;"
#mysql -u root -e "grant all privileges on WordpressCMS.* to 'vagrant'@'localhost';"
#mysql -u root -e "set password for 'vagrant'@'localhost' = password('vagrant');"

# wordpress installation with admin credentials: admin/admin (anders.krarup@zuparecommended.dk)
#echo "Importing local data from previous datadump. This could take a while..."
#mysql -u root WordpressCMS < /vagrant_data/mysql-local.sql
#echo "Local data imported. Please remember to export the WordpressCMS databases into mysql-local.sql if you want the data to persist"
# adjust wordpress to this environment
#echo "Updating wordpress data for localhost:9872"
#mysql -u root WordpressCMS -e "update wp_options set option_value = 'http://localhost:9872' where option_name = 'siteurl' or option_name = 'home';"

service php7.0-fpm restart
service apache2 restart
